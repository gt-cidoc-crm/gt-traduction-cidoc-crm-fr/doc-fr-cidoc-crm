s/^>#[# ]*\(.\+\)/<section descriptiveId="\L\1">/;
/^>.*/d;
/^$/d;
s/^#\+[ ]*\(.*\)/\t<title xml:lang="fr"><![CDATA[[\1]]]><\/title>/;
s/\(^[A-Z]\+.*\)/\t<contents xml:lang="fr"><![CDATA[<p>\1<\/p>]]><\/contents>/g;
s/^\[^\([0-9]\+\)\]:[ ]*\(.*\)/\t<footnotes>\n\t\t<note id="footnote-\1" xml:lang="fr"><![CDATA[<p> \2 <a href="\#footnote-ref-\1">↑<\/a><\/p>\n\t\t]]><\/note>\n\t<\/footnotes>/g;
s/\[^\([0-9]\+\)\]/<sup><a href="#footnote-\1" id="footnote-ref-\1">[\1]<\/a><\/sup>/g;
# ajouter la gestion des tableaux pour 01_05_04… et 01_05_05
s/^| *-\+ *| *-\+ *|.*/TABLE/g;
/^[|].*/d; # efface les autres lignes du tableau qui sont traitées par ailleurs avec pandoc… cf. script md-to-xml-text-conversions-with-table.sh
s/<br>/<\/p><p>/g;
s/<\([/]\{0,1\}\)li>/<\1li>/g;
s/<\([/]\{0,1\}\)ul>/<\1ul>/g;
