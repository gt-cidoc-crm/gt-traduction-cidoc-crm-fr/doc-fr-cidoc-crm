#!/bin/bash
# script to build zip file to package all xml file required to deploy translation on CIDOC-CRM website
# to launch it, just check that this file have 'execute' (x) rights (chmod +x build-zip.sh) and launch it with ./build-zip.sh

# launch sub-script to create xml files required to zip its
source md-to-xml-conversions-entities.sh
source md-to-xml-conversions-properties.sh
source md-to-xml-conversions-texts.sh

datetime=$(date +%Y%m%d_%H%M)
suffix="_fr.zip"
cd ..
zip xml/cidoc_crm_test_v7.1.2@$datetime$suffix \
	entities/*.xml \
	properties/*.xml \
	textes/[a-z]*.xml \
	textes/[*.xml
