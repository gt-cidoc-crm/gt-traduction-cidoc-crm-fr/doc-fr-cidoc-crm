#!/bin/bash

echo 'Conversions des textes en cours… patientez !'
# retrieve files list to process
files=(../textes/0*.md)
# loop for each file
for file in "${files[@]}"; do
	filename_with_xml_extension="${file%.md}.xml"
	sed -f ./toc-md-to-xml-texts.sed "$file" > "$filename_with_xml_extension" && echo "</section>" >> "$filename_with_xml_extension"
done

echo 'Conversions des textes avec tableau en cours… patientez !'
# définir les fichiers à traiter :
files=(../textes/01_05_0[45]*.md) 
for md_file_to_convert in "${files[@]}"; do
	#	echo "${md_file_to_convert}"
	filename_with_xml_extension="${md_file_to_convert%.md}.xml"
	#	echo -e "$filename_with_xml_extension"
	# sed pour supprimer le tableau et pour creer le xml
	sed -f toc-md-to-xml-texts.sed ${md_file_to_convert} > raw.xml
	# split pour découper ligne par ligne split -l 1 --numeric…
	split -l 1 -d raw.xml
	# sed pour supprimer tout sauf le tableau suivi de pandoc -gfm qui permet de convertir le tableau md en html
	sed '/^[^|].*/d' ../textes/01_05_04_a-propos-des-expressions-logiques-utilisees-dans-le-cidoc-crm.md | pandoc -f gfm > table-raw.html
	# suite au split, on remplace le fichier où il y a juste le mot TABLE (substitution faite par le 1er sed) par le tableau html
	grep -rn TABLE x?? | awk -F: '{print $1}' | xargs -I{} mv table-raw.html {}
	# concaténation presque finale
	cat x?? > final.xml
	# suppression de la fermeture CDATA mal placée
	sed 's/\]\]><\/contents>//g;' -i final.xml
	# reste à déplacer la fermeture de CDATA !
	echo ']]></contents></section>' >> final.xml
	mv final.xml "$filename_with_xml_extension"
	# suppression des fichiers de travail
	rm raw.xml x??
done

## Renommage des fichiers
path_where_convert='../textes/'
# source : https://www.baeldung.com/linux/csv-parsing
while IFS=";" read -r xml_name initial_numbered_name
do
	if [ "$initial_numbered_name" != "" ]; then
		from_file="$path_where_convert${initial_numbered_name%.md}.xml"
		mv $from_file "$path_where_convert$xml_name.xml"
	fi
done < ../textes/fr_scheme_texte_md.csv

# check created xml to avoid errors
xml_error=$(xmllint --noout ../textes/*.xml)
if [ $? != 0 ]; then
	echo "les fichiers XML des propriétés comportent des erreurs, corrigez-les avant de relancer la construction XML"
	exit
fi
