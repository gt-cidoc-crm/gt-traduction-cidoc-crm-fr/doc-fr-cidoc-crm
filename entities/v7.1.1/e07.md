> # E7 Activity

> | | |
> | --- | --- |
> | Subclass of: | E5 Event
> | Superclass of: | E8 Acquisition  <br> E9 Move <br> E10 Transfer of Custody  <br> E11 Modification  <br> E13 Attribute Assignment <br> E65 Creation  <br> E66 Formation  <br> E85 Joining <br> E86 Leaving <br> E87 Curation Activity
> | Scope note: | This class comprises actions intentionally carried out by instances of E39 Actor that result in changes of state in the cultural, social, or physical systems documented. <br> This notion includes complex, composite and long-lasting actions such as the building of a settlement or a war, as well as simple, short-lived actions such as the opening of a door.
> | Examples: | the Battle of Stalingrad (Hoyt, 1993) <br> the Yalta Conference (Harbutt, 2010) <br> my birthday celebration 28-6-1995 <br> the writing of “Faust” by Goethe (E65) (Williams, 2020) <br> the formation of the Bauhaus 1919 (E66) (Droste, 2006) <br> calling the place identified by TGN ‘7017998’ ‘Quyunjig’ by the people of Iraq <br> Kira Weber working in glass art from 1984 to 1993 <br> Kira Weber working in oil and pastel painting from 1993
> | In First Order Logic: |  E7(x) ⇒ E5(x)
> | Properties: | P14 carried out by (performed): E39 Actor<br> (P14.1 in the role of: E55 Type)<br> P15 was influenced by (influenced): E1 CRM Entity<br> P16 used specific object (was used for): E70 Thing<br> (P16.1 mode of use: E55 Type)<br> P17 was motivated by (motivated): E1 CRM Entity<br> P19 was intended use of (was made for): E71 Human-Made Thing<br> (P19.1 mode of use: E55 Type)<br> P20 had specific purpose (was purpose of): E5 Event<br> P21 had general purpose (was purpose of): E55 Type<br> P32 used general technique (was technique of): E55 Type<br> P33 used specific technique (was used by): E29 Design or Procedure<br> P125 used object of type (was type of object used in): E55 Type<br> P134 continued (was continued by): E7 Activity<br><br>
