> # E14 Condition Assessment

> | | |
> | --- | --- |
> | Subclass of: | E13 Attribute Assignment
> | Superclass of: |
> | Scope note: | This class describes the act of assessing the state of preservation of an object during a particular period. <br> The condition assessment may be carried out by inspection, measurement or through historical research. This class is used to document circumstances of the respective assessment that may be relevant to interpret its quality at a later stage, or to continue research on related documents. 
> | Examples: | last year’s inspection of humidity damage to the frescos in the St. George chapel in our village (fictitious) <br> the condition assessment of the endband cores of MS Sinai Greek 418 by Nicholas Pickwoad in November 2003 (Honey & Pickwoad, 2010) <br> the condition assessment of the cover of MS Sinai Greek 418 by Nicholas Pickwoad in November 2003 (Honey & Pickwoad, 2010)
> | In First Order Logic: |  E14(x) ⇒ E13(x)
> | Properties: | P34 concerned (was assessed by): E18 Physical Thing <br> P35 has identified (identified by): Ε3 Condition State
