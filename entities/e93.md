> # E93 Presence

> | | |
> | --- | --- |
> | Subclass of: | E92 Spacetime Volume
> | Superclass of: |
> | Scope note: |  	This class comprises instances of E92 Spacetime Volume, whose temporal extent has been chosen in order to determine the spatial extent of a phenomenon over the chosen time-span. Respective phenomena may, for instance, be historical events or periods, but can also be the diachronic extent and existence of physical things. In other words, instances of this class fix a slice of another instance of E92 Spacetime Volume in time.<br>The temporal extent of an instance of E93 Presence typically is predetermined by the researcher so as to focus the investigation particularly on finding the spatial extent of the phenomenon by testing for its characteristic features. There are at least two basic directions such investigations might take. The investigation may wish to determine where something was during some time or it may wish to reconstruct the total passage of a phenomenon’s spacetime volume through an examination of discrete presences. Observation and measurement of features indicating the presence or absence of a phenomenon in some space allows for the progressive approximation of spatial extents through argumentation typically based on inclusion, exclusion and various overlaps.
> | Examples: The Roman Empire on 19 August AD 14<br>Johann Joachim Winkelmann’s whereabouts in December 1775<br>Johann Joachim Winkelmann’s whereabouts from November 19 1755 until April 9 1768
> | In First Order Logic: |  E93(x) ⊃ E92(x)
> | Properties: P164 during (was time-span of): E52 Time Span<br>P166 was a presence of (had presence): E92 Space Time Volume<br>P167 at (was place of): E53 Place<br>P195 was a presence of (had presence): E18 Physical Thing

# E93

| | |
|---|---|
| Sous-classe de |  |
| Super-classe de |  |
| Note d’application : |  |
| Exemples : |  |
| Logique du premier ordre : |  |
| Propriétés : |  |

