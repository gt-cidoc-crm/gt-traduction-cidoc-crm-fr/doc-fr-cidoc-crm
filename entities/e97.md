> # E97 Monetary Amount

> | | |
> | --- | --- |
> | Subclass of: |	E54_Dimension
> | Superclass of: |	
> | Scope note: |	This class comprises quantities of monetary possessions or obligations in terms of their nominal value with respect to a particular currency. These quantities may be abstract accounting units, the nominal value of a heap of coins or bank notes at the time of validity of the respective currency, the nominal value of a bill of exchange or other documents expressing monetary claims or obligations. It specifically excludes amounts expressed in terms of weights of valuable items, like gold and diamonds, and quantities of other non-currency items, like goats or stocks and bonds.<br>Example:<br>Christies’ hammer price for “Vase with Fifteen Sunflowers” (E97) has currency British Pounds (E98)
> | Examples: |
> | In First Order Logic: E97(x) ⊃ E54(x)
> | Properties: 	P180 has currency (was_currency_of): E98 Currency<br>	P181 has amount: E60 Number

# E97_Valeur_monétaire

| | |
|---|---|
| Sous-classe de |  |
| Super-classe de |  |
| Note d’application : |  |
| Exemples : |  |
| Logique du premier ordre : |  |
| Propriétés : |  |

