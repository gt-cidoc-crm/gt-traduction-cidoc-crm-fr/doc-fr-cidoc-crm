> # E72 Legal Object

> | | |
> | --- | --- |
> | Subclass of: | 	E70 Thing
> | Superclass of: | 	E18 Physical Thing<br>E90 Symbolic Object
> | Scope note: |	This class comprises those material or immaterial items to which instances of E30 Right, such as the right of ownership or use, can be applied.<br>This is true for all instances of E18 Physical Thing. In the case of instances of E28 Conceptual Object, however, the identity of an instance of E28 Conceptual Object or the method of its use may be too ambiguous to reliably establish instances of E30 Right, as in the case of taxa and inspirations. Ownership of corporations is currently regarded as out of scope of the CIDOC CRM. 
> | Examples: | 	<br>the Cullinan diamond (E19) (Scarratt and Shor, 2006)<br>definition of the CIDOC Conceptual Reference Model Version 5.0.4 (E73) (ISO 21127: 2004)
> | In First Order Logic: |  E72(x) ⊃ E70(x)
> | Properties: P104 is subject to (applies to): E30 Right<br>P105 right held by (has right on): E39 Actor

# E72_Objet_juridique

| | |
|---|---|
| Sous-classe de | E70_Chose |
| Super-classe de | E18_Chose_matérielle<br>E90_Objet_symbolique |
| Note d’application : |  Cette classe comprend les items physiques ou immatériels auxquels peuvent s'apppliquer des instances de E30_Droit, comme le droit de propriété ou d'usage.<br>Ceci est vrai pour toute instance de E18_Chose_matérielle. Cependant, dans le cas d'instances de E28_Objet_conceptuel, leur identité ou la méthode de leur usage peuvent être trop ambigues pour établir de manière fiable les instances de E30_Droit. C'est par exemple le cas pour les taxons et les source d’inspirations. La propriété des entreprises est actuellement considérée comme hors du champ d'application du CIDOC CRM. |
| Exemples : | le diamant Cullinan (E19) (Scarratt et Shor, 2006)<br>la définition du CIDOC Conceptual Reference Model dans sa Version 5.0.4 (E73) (ISO 21127: 2004) |
| Logique du premier ordre : | E72(x) ⊃ E70(x) |
| Propriétés : P104_est_soumis.e_à_(s'applique_à): E30_Droit<br>P105_droit_détenu_par_(détient_un_droit_sur): E39_Agent  |

