> # E55_Type

> | | |
> | --- | --- |
> | Subclass of: | E28 Conceptual Object
> | Superclass of: | E56 Language<br>E57 Material<br>E58 Measurement Unit<br>E99 Product Type
> | Scope note: | This class comprises concepts denoted by terms from thesauri and controlled vocabularies used to characterize and classify instances of CIDOC CRM classes. Instances of E55_Type represent concepts in contrast to instances of E41_Appellation which are used to name instances of CIDOC CRM classes.<br>E55_Type is the CIDOC CRM’s interface to domain specific ontologies and thesauri. These can be represented in the CIDOC CRM as subclasses of E55_Type, forming hierarchies of terms, i.e., instances of E55_Type linked via P127 has broader term (has narrower term): E55_Type. Such hierarchies may be extended with additional properties. 
> | Examples: | weight, length, depth [types for instances of E54]<br>portrait, sketch, animation [types for instances of E36]<br>French, English, German (E56)<br>excellent, good, poor [types for instances of E3]<br>Ford Model T, chop stick [types for instances of E22]<br>cave, doline, scratch [types for instances of E26]<br>poem, short story [types for instances of E33]<br>wedding, earthquake, skirmish [types for instances of E5]
> | In First Order Logic: | E55(x) ⇒ E28(x)
> | Properties: | P127 has broader term (has narrower term): E55_Type<br>P150 defines typical parts of (define typical wholes for): E55_Type

# E55_Type

| | |
|---|---|
| Sous-classe de | E28_Objet_conceptuel |
| Super-classe de | E56 Language<br>E57_Matériau<br>E58_Unité_de_mesure| E99_Type_de_produit
| Note d’application : | Cette classe comprend les concepts qui sont désignés par des termes provenant de thesauri et vocabulaires contrôlés utilisés pour décrire et répertorier les instances de classe du CIDOC-CRM.Les instances de E55_Type représentent des concepts à la différence des instances de E41_Appellation qui sont utilisées pour nommer des instances de classes du CIDOC-CRM.<br>E55_Type est le point d'entrée du CIDOC-CRM vers les ontologies et les thesauri spécifiques d’un domaine. Ceux-ci peuvent être représentés dans le CIDOC CRM comme des sous-classes de E55_Type, formant des hiérarchies de termes, c’est-à-dire des instances de E55_Type liées par P127_a_pour_terme_générique_(a_pour_terme_spécifique): E55_Type. De telles hiérarchies peuvent être étendues avec des propriétés additionnelles.|
| Exemples : | poids, longueur, profondeur [types pour des instances de E54]<br>portrait, croquis, animation [types pour des instances de E36]<br>français, anglais, allemand (E56)<br>excellent, bon, mauvais [types pour des instances de E3]<br>Ford T, baguette (couvert) [types pour des instances de E22]<br>grotte, doline, égratignures [types pour des instances de E26]<br>poème, nouvelle [types pour des instances de E33]<br>mariage, tremblement de terre, escarmouche [types pour des instances de E5]|
| Logique du premier ordre : | E55(x) ⇒ E28(x) |
| Propriétés : | P127_a_pour_terme_générique_(a_pour_terme_spécifique): E55_Type<br>P150 définit les parties typiques de (définit l’ensemble typique de) : E55_Type| 

