> # E61 Time Primitive

> | | |
> | --- | --- |
> | Subclass of: | E41_Appellation<br>E59 Primitive Value
> | Superclass of: |
> | Scope Note: | This class comprises instances of E59 Primitive Value for time that should be implemented with appropriate validation, precision and references to temporal coordinate systems to express time in some context relevant to cultural and scientific documentation.<br>Instantiating different instances of E61 Time Primitive relative to the same instance of E52 Time Span allows for the expression of multiple opinions/approximations of the same phenomenon. When representing different opinions/approximations of the E52 Time Span of some E2 Temporal Entity, multiple instances of E61 Time Primitive should be instantiated relative to one E52 Time Span. Only one E52 Time Span should be instantiated since there is only one real phenomenal time extent of any given temporal entity.<br>The instances of E61 Time Primitive are not considered as elements of the universe of discourse that the CIDOC CRM aims at defining and analysing. Rather, they play the role of a symbolic interface between the scope of this model and the world of mathematical and computational manipulations and the symbolic objects they define and handle.<br>Therefore, they must not be represented in an implementation by a universal identifier associated with a content model of different identity. In a concrete application, it is recommended that the primitive value system from a chosen implementation platform and/or data definition language be used to substitute for this class.
> | Examples: | “1994 – 1997”<br>“13th May 1768”<br>“2000/01/01 00:00:59.7”<br>“85th century BCE”
> | In First Order Logic: | E61(x) ⇒ E41(x)<br>E61(x) ⇒ E59(x)
> | Properties: | P170 defines time (time is defined by): E52 Time Span


# E61_Primitive_temporelle

| | |
|---|---|
| Sous-classe de | E41_Appellation<br>E59_Valeur_primitive |
| Superclass of: |
| Note d’application : | Cette classe comprend des instances de E59_Valeur_primitive qui doivent exprimer des données temporelles dans un contexte pertinent par rapport à la documentation culturelle et scientifique. Dans cette optique, les données temporelles doivent être implémentées avec la validation, la précision et les références appropriées à des systèmes de coordonnées temporelles.<br>L’instanciation de différentes instances de E61_Primitive_temporelle relatives à la même instance de E52_Laps_de_temps permet d’exprimer plusieurs opinions/approximations d’un même phénomène. Si on veut représenter plusieurs opinions/approximations du E52_Laps_de_temps d’une E2_Entité_temporelle, il faut instancier plusieurs instances de E61_Primitive_temporelle pour ce E52_Laps_de_temps. Par contre, seul un E52_Laps_de_temps sera instancié, puisqu’il n’existe qu’un phénomène réel (et donc une seule extension temporelle) pour une entité temporelle.<br>Les instances de E61_Primitive_temporelle ne sont pas considérées comme des éléments faisant partie de l’univers du discours que le CIDOC CRM ambitionne de définir et d’analyser. Ils jouent plutôt le rôle d’interface symbolique entre le champ d’application de ce modèle et le monde des manipulations mathématiques et informatiques, ainsi que les objets symboliques qu’ils définissent et manipulent.<br> Par conséquent, ils ne doivent pas être représentés dans une implémentation par un identifiant universel associé à un modèle de contenu d’une identité différente. Dans une application concrète, il est recommandé d’utiliser le système de valeurs primitives d’une plate-forme d’implémentation choisie et/ou d’un langage de définition des données pour remplacer cette classe. |
| Exemples : | “1994 – 1997”<br>“13 mai 1768”<br>“01/01/2000 00:00:59.7”<br>“85ème siècle avant J.-C." (n.d.t : on parlera en français plutôt du milieu du 9ème millénaire avant J.-C.) |
| Logique du premier ordre : | E61(x) ⇒ E41(x)<br>E61(x) ⇒ E59(x) |
| Propriétés : | P170_temps_défini_par_(définit_le_temps) : E52_Laps_de_temps |

