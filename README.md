# Doc fr CIDOC CRM
[![Build status](https://gitlab.huma-num.fr/gt-cidoc-crm/gt-traduction-cidoc-crm-fr/doc-fr-cidoc-crm/badges/translate/pipeline.svg)](https://gitlab.huma-num.fr/gt-cidoc-crm/gt-traduction-cidoc-crm-fr/doc-fr-cidoc-crm/pipelines)

**[Scroll down for English](#english-informations)**

L'objectif est de traduire en français la documentation du CIDOC CRM la plus à jour.<br> 
Ce groupe a été formé suite à l'Ecole thématique "Données Interopérables pour le Patrimoine"  [DONIPAT](https://masa.hypotheses.org/donipat) organisée par le consortium MASA (octobre 2019).<br>
Nous avons commencé avec la version V6.2.7, disponible malheureusement qu'au [format docx](http://www.cidoc-crm.org/versions-of-the-cidoc-crm).<br/> 
La version V7.0 est disponible depuis le 20 juin 2020, puis la [version 7.1](http://www.cidoc-crm.org/sites/default/files/CIDOC%20CRM_v.7.1%20%5B8%20March%202021%5D.doc) est sortie le 8 mars 2021.<br/>
La [version 7.1.1](http://www.cidoc-crm.org/sites/default/files/cidoc_crm_version_7.1.1.docx) sortie en avril 2021, est devenu, quelques temps, la **version de référence** pour les traductions, d'autant qu'elle était la version "ultime" pour la demande de révision ISO… avant que la [version 7.1.2](https://cidoc-crm.org/Version/version-7.1.2) ne vienne compléter quelques propriétés (cf. [CR réunion du 25/2/2022](https://gitlab.huma-num.fr/gt-cidoc-crm/gt-traduction-cidoc-crm-fr/doc-fr-cidoc-crm/-/wikis/2022-02-25-Réunion))

# Comment participer?

CONSULTER le [WIKI](https://gitlab.huma-num.fr/gt-cidoc-crm/gt-traduction-cidoc-crm-fr/doc-fr-cidoc-crm/-/wikis/home) pour voir comment travailler.<br>
En cas de problème, vous pouvez poser une question via un [_Ticket_](https://gitlab.huma-num.fr/gt-cidoc-crm/gt-traduction-cidoc-crm-fr/doc-fr-cidoc-crm/-/issues) (*issue* en anglais), ou contacter @bdavid ou @rkrummeich.<br>
Nous utilisons une **liste de diffusion** (infos de réunions…), n'hésitez pas à demander (@bdavid) qu'on vous y ajoute.

## Planning
* Mise en place : mi décembre 2019
* Invitations des contributeurs et contributrices : printemps 2020
* Début travail collaboratif : printemps 2020… été 2020 car nous avons attendu la v7
* Depuis mai 2021 : des réunions hebdomadaires ont lieu le vendredi de 15h à 17h
* Planning de rentrée 2022 : à venir

## Liste des contributeur·ice·s enregistré·e·s et autres personnes qui suivent le projet
Visible également (surtout) [ici](https://gitlab.huma-num.fr/gt-cidoc-crm/gt-traduction-cidoc-crm-fr/doc-fr-cidoc-crm/-/project_members)

| Prénom Nom | email | institut/laboratoire d’appartenance | lien gitlab |
| ------ | ------ | ------ | ------ |
| Anais Guillem       | anais.guillem@gmail.com        | Laboratoire de Recherche des Monuments Historiques/ MAP-CNRS | @aguillem |
| Bulle Tuil Leonetti | bulle.leonetti@inha.fr                                | InVisu, CNRS-INHA (https://invisu.cnrs.fr/) | @btuilleonetti |
| Olivier Marlet      | olivier.marlet@univ-tours.fr                          | UMR7324 CITERES-LAT, CNRS-Université de Tours, Consortium MASA | @omarlet |
| Raphaëlle Krummeich | raphaelle.krummeich@univ-rouen.fr                     | [Université de Rouen](https://www.univ-rouen.fr/) | @rkrummeich |
| Muriel Van Ruymbeke | murielvr@skynet.be                                    | C2DH – Université de Luxembourg (https://www.c2dh.uni.lu/) | @mvanruymbeke |
| [Bertrand David](https://www.arar.mom.fr/content/david-bertrand) | bertrand.david@cnrs.fr | CNRS-INSHS-MOM- [laboratoire ArAr](https://arar.mom.fr) | @bdavid |
| Juliette Hueber     | juliette.hueber@inha.fr                               | InVisu, CNRS-INHA (https://invisu.cnrs.fr/) | @jhueber |
| Vincent Alamercery  | vincent.alamercery@ens-lyon.fr                        | CNRS-INSHS-[LARHRA](http://larhra.ish-lyon.cnrs.fr/membre/54) | @valamercery |
| Romain Boissat      | romain.boissat@mom.fr                                 | CNRS-Maison de l'Orient et de la Méditérannée de Lyon | @rboissat |
| Melanie Roche       | melanie.roche@bnf.fr                                  | [BnF](https://experts.bnf.fr/page_personnelle/melanie-roche) | @mroche |
| Aurelia Vasile      | aurelia.vasile@uca.fr                                 |  | @avasile |
| Emmanuelle Morlock  | emmanuelle.morlock@mom.fr                             |  | @emorlock |
| Hélène Jamet        | helene.jamet@mom.fr                                   |  |
| Danielle Ziebelin   | Danielle.Ziebelin@imag.fr                             |  |
| Diane Rego          | diane.rego@unicaen.fr                                 |  |
| Adeline Levivier    | adeline.levivier@gmail.com; adeline.levivier@efeo.net |  |
| George Bruseker     | george.bruseker@gmail.com                             |  |
| Anne-Violaine Szabados | anne-violaine.szabados@cnrs.fr                     |  | @aszabados |
| Marlène Nazarian    | marlene.nazarian@cnrs.fr                              |  | |
| Stephen Hart        | stephen.h.hart@gmail.com                              |  | @shart |
| Frédéric Bricaud    | frederic.bricaud@banq.qc.ca                           |  | @fbricaud |
| Philippe Michon     | illipmich@gmail.com                                   |  | @pmichon |
| Antoine Gros        | antoine.gros@map.cnrs.fr                              | MAP-CNRS | @agros |
| Olivier Malavergne  | olivier.malavergne@culture.gouv.fr | Laboratoire de Recherche des Monuments Historiques | |

## Résultat
Ce projet met en œuvre de l'intégration continue, c'est à dire que pour chaque modification enregistrée (`git commit`), des mécanismes automatiques s'exécutent pour créer des pages web… accessibles à l'url : https://cidoc-crm-fr.mom.fr/. <br> Pour plus d'informations, consulter le ticket #5.

## Veille scientifique et bibliographique
Le projet collecte une bibliographie en lien avec CIDOC CRM et les recherches associées. <br>
Pour pariciper à la [bibliothèque de groupe Zotero](https://www.zotero.org/groups/4641239), envoyer un email à anais.guillem@gmail.com.

# English informations
<details><summary><strong>Click this to collapse/fold</strong></summary>
<br><strong>Project initiative</strong><br>
The French speaking research community network's initiative started during DONIPAT CNRS thematic school (2019) organized by MASA Consortium.<br>
The training sessions about CIDOC CRM with the Game led by G. Bruseker and A. Guillem facilitated the appropriation of the ontology by different groups of French researchers.<br>
The need for an updated translation came up as future developments for a more efficient dissemination of the know-how in modelling with CIDOC CRM in different scientific fields within French speaking Digital Humanities (DH) communities.<br>
The objective is to collectively provide an updated translation of the CIDOC CRM following the FAIR principles from the start:
<ul>
<li>(F) Findable: a git public repository on the French-speaking HumaNum infrastructure [Gitlab project repository](https://gitlab.huma-num.fr/gt-cidoc-crm/gt-traduction-cidoc-crm-fr/doc-fr-cidoc-crm); a continuous integration of the translation in a [HTML page *[WORK IN PROGRESS]*](https://cidoc-crm-fr.mom.fr).</li>
<li>(A) Accessible: methods are described in the wiki and meeting reports are published after weekly open meetings, a published agenda & translation flows organise translation priorities etc.</li>
<li>(I) Interoperable: a markdown syntax is used for all the translation files, published in html, code is shared  for the translation project.</li>
<li>(R) Reusable: repository is public under licences CC-BY-SA 4.0 International & ODbL v1.0.</li>
</ul>
<strong>The method</strong><br>
Institutionally, the translation initiative is supported by the MASA Consortium but the project is managed collectively by individual researchers representing various cultural or research institutions (see [project participants table](https://gitlab.huma-num.fr/gt-cidoc-crm/gt-traduction-cidoc-crm-fr/doc-fr-cidoc-crm/-/blob/translate/README.md)). <br/> Regarding the translation/validation method, translations tracked in the labelled issues board are proposed by individuals and pushed asynchronously into the Gitlab repository. The validation is a synchronous collective process held during weekly meetings, based on logical groups of entities and properties (see, for example [E1 logical group](https://gitlab.huma-num.fr/gt-cidoc-crm/gt-traduction-cidoc-crm-fr/doc-fr-cidoc-crm/-/blob/translate/stats/graphE1.md)).<br> In July 2022, more than 60% of entities and properties are translated. So far in July 2022, 28 meetings took place on Fridays 3PM (+1GMT or +2GMT), on Renater [Rendez-vous](https://rendez-vous.renater.fr) platform.<br> 
<br><strong>Intended purpose</strong><br>
The collective translation effort gives the opportunity to create a positive learning environment for deepening the knowledge of the CIDOC CRM by the group of translators. <br>The weekly meetings allow the group to share and discuss scientific updates about CIDOC CRM and related topics, and build a French speaking community of CIDOC CRM expert users.
</details>
