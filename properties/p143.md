> # P143 joined (was joined by)

> | | |
> | --- | --- |
> | Domain: | E85 Joining
> | Range: | E39 Actor
> | Subproperty of: | E5 Event. P11 had participant (participated in): E39 Actor
> | Superproperty of: |
> | Quantification: | many to many, necessary (1,n:0,n)
> | Scope note: | This property identifies the instance of E39 Actor that becomes member of an instance of E74 Group in an instance of E85 Joining.<br>Joining events allow for describing people becoming members of a group with the more detailed path E74 Group, P144i gained member by, E85 Joining, P143 joined, E39 Actor, compared to the shortcut offered by P107 has current or former member (is current or former member of).
> | Examples: |     • The election of Sir Isaac Newton as Member of Parliament to the Convention Parliament of 1689 (E85) joined Sir Isaac Newton (E21) (Iliffe, 2013)<br>• The inauguration of Mikhail Sergeyevich Gorbachev as leader of the Union of Soviet Socialist Republics (USSR) in 1985 (E85) joined Mikhail Sergeyevich Gorbachev (E21) (Galeotti, 1997)<br>• The implementation of the membership treaty January 1. 1973 between EU and Denmark (E85) joined Denmark (E74)
> | In First Order Logic: | P143(x,y) ⇒ E85(x)<br>P143(x,y) ⇒ E39(y)<br>P143(x,y) ⇒ P11(x,y)
> | Properties: |

# P143_a_fait_entrer_(est_entré_par)

| | |
|---|---|
| Domaine |  |
| Co-domaine |  |
| Sous-propriété de |  |
| Super-propriété de |  |
| Quantification : |  |
| Note d'application : |  |
| Exemples : |  |
| Logique du premier ordre: |  |
| Propriétés: |  |

