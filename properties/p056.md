> # P56 bears feature (is found on)

> | | |
> | --- | --- |
> | Domain: | E19 Physical Object
> | Range: | E26 Physical Feature
> | Subproperty of: | E18 Physical Thing. P46 is composed of (forms part of): E18 Physical Thing
> | Superproperty of: |
> | Quantification: | one to many, dependent (0,n:1,1)
> | Scope note: | This property links an instance of E19 Physical Object to an instance of E26 Physical Feature that it bears.<br>An instance of E26 Physical Feature can only exist on one object. One object may bear more than one E26 Physical Feature. An instance of E27_Site should be considered as an instance of E26 Physical Feature on the surface of the Earth.<br>An instance B of E26 Physical Feature being a detail of the structure of another instance A of E26 Physical Feature can be linked to B by use of the property P46 is composed of (forms part of). This implies that the subfeature B is P56i is found on the same E19 Physical Object as A.<br>This property is a shortcut. A more detailed representation can make use of the fully developed (i.e., indirect) path E19 Physical Object, through, P59 has section, E53 Place, P53i is former or current location of, to, E26 Physical Feature.
> | Examples: |     • silver cup 232 (E22) bears feature 32 mm scratch on silver cup 232 (E26) (fictitious)
> | In First Order Logic: | P56(x,y) ⇒E19(x)<br>P56(x,y) ⇒ E26(y)<br>P56(x,y) ⇒ P46(x,y)<br>P56(x,y) ⇐ (∃z) [E53(z) ˄ P59(x,z) ˄ P53i(z,y)]
> | Properties: |

# P56_présente_comme_caractéristique_(se_trouve_sur)
 
| | |
|---|---|
| Domaine | `E19_Objet_matériel`
| Co-domaine | `E26_Caractéristique_matérielle`
| Sous-propriété de | `E18_Chose_matérielle`. `P46_est_composé·e_de_(fait_partie_de)` : `E18_Chose_matérielle`
| Super-propriété de |
| Quantification : | un-à-plusieurs, dépendant (0,n:1,1)
| Note d'application : | Cette propriété relie une instance de `E19_Objet_matériel` à une instance de `E26_Caractéristique_matérielle` qu’il porte.<br>Une instance de `E26_Caractéristique_matérielle` ne peut exister que sur un seul objet. Un objet peut porter plus d’une `E26_Caractéristique_matérielle`. Une instance de `E27 site` doit être considérée comme une instance de `E26_Caractéristique_matérielle` sur la surface de la Terre.<br>Une instance B de `E26_Caractéristique_matérielle` qui serait un détail de la structure d’une autre instance A de `E26_Caractéristique_matérielle` peut être reliée à B par l’utilisation de la propriété `P46_est_composé·e_de_(fait_partie_de)`. Ceci implique que la sous-caractéristique B `P56i_se_trouve_sur` le même `E19_Objet_matériel` que A.<br>Cette propriété est un raccourci. Une représentation plus détaillée utiliserait le chemin complet (c’est-à-dire indirect) : `E19_Objet_matériel` `P59_a_pour_zone` `E53_Lieu` `P53i est la localisation actuelle ou antérieure de` `E26_Caractéristique_matérielle`.
| Exemples : | la coupe d’argent 232 (E22) présente pour caractéristique une égratignure de 32 mm sur la coupe d’argent 232 (E26) (exemple fictif)
| Logique du premier ordre: | P56(x,y) ⇒E19(x)<br>P56(x,y) ⇒ E26(y)<br>P56(x,y) ⇒ P46(x,y)<br>P56(x,y) ⇐ (∃z) [E53(z) ˄ P59(x,z) ˄ P53i(z,y)]
| Propriétés: |

