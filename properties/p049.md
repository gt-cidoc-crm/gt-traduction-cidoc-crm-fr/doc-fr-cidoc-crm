> # P49 has former or current keeper (is former or current keeper of)

> | | |
> | --- | --- |
> | Domain: |		E18 Physical Thing
> | Range: |		E39 Actor
> | Subproperty of: |
> | Superproperty of: |	E18 Physical Thing. P50 has current keeper (is current keeper of): E39 Actor<br>E78 Curated Holding.P109 has current or former curator (is current or former curator of):E39 Actor
> | Quantification: |	many to many (0,n:0,n)
> | Scope note: |	This property identifies the instance of E39 Actor who has or has had custody of an instance of E18 Physical Thing at some time. This property leaves open the question if parts of this physical thing have been added or removed during the time-spans it has been under the custody of this actor, but it is required that at least a part which can unambiguously be identified as representing the whole has been under this custody for its whole time. The way, in which a representative part is defined, should ensure that it is unambiguous who keeps a part and who the whole and should be consistent with the identity criteria of the kept instance of E18 Physical Thing.<br>The distinction with P50 has current keeper (is current keeper of) is that P49 has former or current keeper (is former or current keeper of) leaves open the question as to whether the specified keepers are current.<br>This property is a shortcut for the more detailed path from E18 Physical Thing through ‘P30i custody transferred through’, E10 Transfer of Custody, ‘P28 custody surrendered by’ or ‘P29 custody received by’ to E39 Actor.
> | Examples: |	<br>The paintings from The Iveagh Bequest  (E18) has former or current keeper  Secure Deliveries Inc. (E74)
> | In First Order Logic: | P49(x,y) ⇒ E18(x)<br>P49(x,y) ⇒ E39(y)<br>P49(x,y) ⇐ (∃z) [E10(z) ˄ P30i(x,z) ˄ [P28(z,y) ˅ P29(z,y) ]]
> | Properties: |

# P49_a_pour_détenteur·rice_actuel·le_ou_antérieur·e_(est_détenteur·rice_actuel·le_ou_antérieur·e_de)

| | |
|---|---|
| Domaine | E18_Chose_matérielle |
| Co-domaine | E39_Acteur·rice·x |
| Sous-propriété de |  |
| Super-propriété de | E18_Chose_matérielle. P50_a_pour_détenteur·rice_actuel·le_(est_détenteur·rice_actuel·le_de) : E39_Acteur·rice·x<br>E78_Collection. P109_a_pour_conservateur·rice·x_actuel·le_ou_antérieur·e_(est_conservateur·rice·x_actuel·le_ou_antérieur·e_de) : E39_Acteur·rice·x |
| Quantification : | plusieurs-à-plusieurs (0,n:0,n) |
| Note d'application : | Cette propriété identifie l’instance de E39_Acteur·rice·x qui a ou a eu la conservation d’une instance de E18_Chose_matérielle à  un moment donné. Cette propriété n’indique pas si des éléments ont été ajoutés ou retirés de la chose matérielle durant la période où elle a été conservé par cet·te acteur·rice·x. Par contre, elle requiert qu’au moins une partie identifiée sans ambiguïté comme représentant l'ensemble ait été conservé par cet·te acteur·trice·x durant la période en question. La façon dont est déterminée cette partie représentative doit garantir qu’il n’y a pas d’ambiguïté quant au détenteur·rice d’une partie et au détenteur·rice du tout, et doit également être conforme avec ce qui définit l’identité de l’instance de E18_Chose_matérielle conservée.<br>La distinction avec P50_a_pour_détenteur·rice_actuel·le_(est_détenteur·rice_actuel·le_de) repose sur le fait que P49_a_pour_détenteur·rice_actuel·le_ou_antérieur·e_(est_détenteur·rice_actuel·le_ou_antérieur·e_de) laisse ouverte la question de savoir si ce·tte dernier·e l'est encore.<br>Cette propriété est un raccourci des chemins détaillés qui relient E18_Chose_matérielle à E39_Acteur·rice·x par : P30i a transféré la conservation par E10 Transfert de conservation, P28 a retiré la conservation à, ou P29 a confié la conservation à E39_Acteur·rice·x. |
| Exemples : |<br>les peintures du Legs Iveagh (E78) a pour détenteur·rice actuel·le ou antérieur·e (est détenteur·rice actuel·le ou antérieur·e de) Secure Deliveries Inc. (E74) |
| Logique du premier ordre: | P49(x,y) ⇒ E18(x)<br>P49(x,y) ⇒ E39(y)<br>P49(x,y) ⇐ (∃z) [E10(z) ˄ P30i(x,z) ˄ [P28(z,y) ˅ P29(z,y) ]] |
| Propriétés: |  |

