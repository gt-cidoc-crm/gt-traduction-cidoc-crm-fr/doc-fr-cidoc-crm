> # P112 diminished (was diminished by)

> | | |
> | --- | --- |
> | Domain: | E80 Part Removal
> | Range: | E24 Physical Human-Made Thing
> | Subproperty of: | E11_Modification. P31 has modified (was modified by): E18 Physical Thing
> | Superproperty of: |
> | Quantification: | many to many, necessary (1,n:0,n)
> | Scope note: | This property identifies the instance E24 Physical Human-Made Thing that was diminished by an instance of E80 Part Removal.<br>Although an instance of E80 Part removal activity normally concerns only one instance of E24 Physical Human-Made Thing, it is possible to imagine circumstances under which more than one item might be diminished by a single instance of E80 Part Removal activity. 
> | Examples: |     • the coffin of Tut-Ankh-Amun (E22) was diminished by The opening of the coffin of Tut-Ankh-Amun (E80) (Carter, 2014)
> | In First Order Logic: | P112(x,y) ⇒ E80(x)<br>P112(x,y) ⇒ E24(y)<br>P112(x,y) ⇒ P31(x,y)
> | Properties: |

# P112

| | |
|---|---|
| Domaine |  |
| Co-domaine |  |
| Sous-propriété de |  |
| Super-propriété de |  |
| Quantification : |  |
| Note d'application : |  |
| Exemples : |  |
| Logique du premier ordre: |  |
| Propriétés: |  |

> # P113 removed (was removed by)
