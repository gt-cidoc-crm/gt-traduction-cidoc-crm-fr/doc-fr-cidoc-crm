> # P175 starts before or with the start of (starts after or with the start of)

> | | |
> | --- | --- |
> | Domain: | E2 Temporal Entity
> | Range: | E2 Temporal Entity
> | Subproperty of: | E2 Temporal Entity. P174 starts before the end of (ends after the start of): E2 Temporal Entity 
> | Superproperty of: | E2 Temporal Entity. P176 starts before the start of (starts after the start of): E2 Temporal Entity 
> | Quantification: | many to many (0,n:0,n)
> | Scope note: | This property specifies that the temporal extent of the domain instance A of E2 Temporal Entity starts before or simultaneously with the start of the temporal extent of the range instance B of E2 Temporal Entity.<br>In other words, if A = [Astart, Aend] and B = [Bstart, Bend], we mean Astart ≤ Bstart is true.<br>This property is part of the set of temporal primitives P173 – P176, P182 – P185.<br>This property corresponds to a disjunction (logical OR) of the following Allen temporal relations [Allen, 1983]: {before, meets, overlaps, starts, started-by, contains, finished-by, equals}<br>In a model with fuzzy borders, this property will not be transitive.<br>Figure 12: Temporal entity A starts before or with the start of temporal entity B. Here A is longer than B<br>Figure 13: Temporal entity A starts before or with the start of temporal entity B. Here A is shorter than B 
> | Example: |
> | In First Order Logic: |<br>P175(x,y) ⇒ E2(x)<br>P175(x,y) ⇒ E2(y)<br>P175(x,y) ⇒  P174(x,y)
> | Properties: |

# P175 commence avant ou avec le début de (commence après ou avec le début de)

| | |
|---|---|
| Domaine | E2_Entité_temporelle |
| Co-domaine | E2_Entité_temporelle |
| Sous-propriété de | E2_Entité_temporelle. P176 commence avant le début de (commence après le début de): E2_Entité_temporelle |
| Super-propriété de |  |
| Quantification : |  many to many (0,n:0,n)|
| Note d'application : | Cette propriété spécifie que l'extension temporelle d'une instance A de E2_Entité_temporelle commence avant ou simultanément avec le début de l'extension temporelle d'une instance B de E2_Entité_temporelle.<br>En d'autres termes, si A = [Adébut, Afin] and B = [Bdébut, Bfin], on veut dire que Adébut ≤ Bdébut est vrai.<br>Cette propriété appartient à l'ensemble des primitives temporelles P173 – P176, P182 – P185.<br>Cette propriété correspond à la disjonction (OU en logique) des relations temporelles d'Allen suivantes:[Allen, 1983]: {avant, rencontre, recouvre, commence, commencé par, contient, terminé par, égale}<br>Dans un modèle avec des limites floues, cette propriété ne sera pas transitive.<br>Figure 12: L'entité temporelle A commence avant ou avec le début de l'entité temporelle B. Ici A est plus long que B<br>Figure 13: L'entité temporelle A commence avant ou avec le début de l'entité temporelle B. Ici A est plus court que B   |
| Exemples : |  |
| Logique du premier ordre: |  <br>P175(x,y) ⇒ E2(x)<br>P175(x,y) ⇒ E2(y)<br>P175(x,y) ⇒  P174(x,y)
| Propriétés: |  |

