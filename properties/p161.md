> # P161 has spatial projection (is spatial projection of)

> | | |
> | --- | --- |
> | Domain: | E92 Spacetime Volume 
> | Range: | E53 Place
> | Subproperty of: | E4 Period.  P7 took place at (witnessed) E53 Place
> | Superproperty of: |
> | Quantification: |  one to many, necessary, dependent (1,n:0,n)
> | Scope note: | This property associates an instance of an instance of E92 Spacetime Volume with an instance of E53 Place that is the result of the spatial projection of the instance of the E92 Spacetime Volume on a reference space.<br>In general, there can be more than one useful reference space (for reference space see P156 occupies and P157 is at rest relative to) to describe the spatial projection of a spacetime volume, for example, in describing a sea battle, the difference between the battle ship and the seafloor as reference spaces. Thus, it can be seen that the projection is not unique.<br>The spatial projection is the actual spatial coverage of a spacetime volume, which normally has fuzzy boundaries except for instances of E92 Spacetime Volumes which are geometrically defined in the same reference system as the range of this property are an exception to this and do not have fuzzy boundaries. Modelling explicitly fuzzy spatial projections serves therefore as a common topological reference of different spatial approximations rather than absolute geometric determination, for instance for relating outer or inner spatial boundaries for the respective spacetime volumes.<br>In case the domain of an instance of P161 has spatial projection is an instance of E4 Period, the spatial projection describes all areas that period was ever present at, for instance, the Roman Empire.<br>This property is part of the fully developed path from E18 Physical Thing through P196 defines, E92 Spacetime Volume, P161 has spatial projection, which in turn is implied by P156 occupies (is occupied by).<br>This property is part of the fully developed path from E4 Period through P161 has spatial projection, E53 Place, P89 falls within (contains) to E53 Place, which in turn is shortcut by P7 took place at (witnessed). 
> | Example: |     • The Roman Empire has spatial projection all areas ever claimed by Rome. (Clare, 1992)
> | In First Order Logic: | P161(x,y) ⇒ E92(x)<br>P161(x,y) ⇒ E53(y)
> | Properties: |

# P161_a_pour_projection_spatiale_(est_la_projection_spatiale_de)

| | |
|---|---|
| Domaine | E92_Volume_spatio-temporel |
| Co-domaine | E53_Lieu |
| Sous-propriété de | E4_Période.  P7 a eu lieu dans (a été témoin de) E53_Lieu |
| Super-propriété de |  |
| Quantification : | un à plusieurs, nécessaire, dépendant (1,n :0,n) |
| Note d'application : | Cette propriété associe une instance de E92_Volume_spatio-temporal avec une instance de E53_Lieu qui est le résultat de la projection spatiale de l’instance de E92_Volume_spatio-temporel sur un espace de référence.<br>En général, il peut exister plus d’un espace de référence pertinent (voir P156_occupe et P157_a_pour_référentiel_spatial) pour décrire la projection spatiale d’un volume spatio-temporel. Par exemple, lorsque l’on décrit une bataille navale, on a le choix entre utiliser comme espace de référence un navire impliqué ou le plancher océanique. Par conséquent, on voit bien que la projection n’est pas unique.<br>La projection spatiale est l’étendue spatiale réelle d'un volume spatio-temporel, qui a normalement des limites floues, sauf pour les instances de E92_Volume_spatio-temporel qui sont géométriquement définies dans le même système de référence que le co-domaine de cette propriété. Ces instances sont une exception et n'ont pas de limites floues. Modéliser explicitement des projections spatiales floues sert, par conséquent, de référence topologique commune pour différentes approximations spatiales plus que comme une localisation géométrique absolue. C’est le cas par exemple en ce qui concerne les limites spatiales extérieures ou intérieures des volumes spatio-temporels respectifs.<br>Pour le cas où le domaine d’une instance de P161_A_pour_projection_spatiale est une instance de E4_Période, la projection spatiale décrit tout l’espace dans lequel cette période a été présente, par exemple durant l’Empire romain.<br>Cette propriété est une partie du chemin plus complet partant de E18_Chose matérielle à travers P196_définit, E92_Volume spatio-temporel, P161_a_pour_projection_spatiale, qui à son tour est concernée (impliquée ?) par P156_occupe (est occupé par).<br>Cette propriété est aussi une partie du chemin plus complet partant de E4_Période et passant par P161_a_pour_projection_spatiale, E53_Lieu, P89_s’insère_dans_(contient) E53_Lieu, qui à son tour est raccourci par P7_a_eu_lieu_à_(a_été_témoin_de). |
| Exemples : | L'Empire romain a pour projection spatiale tous les territoires ayant été un jour déclarés comme relevant du territoire de Rome. (Clare & Edwards, 1992) |
| Logique du premier ordre : | P161(x,y) ⇒ E92(x)<br>P161(x,y) ⇒ E53(y)  |
| Propriétés : |  |

