> # P122 borders with

> | | |
> | --- | --- |
> | Domain: | E53 Place
> | Range: | E53 Place
> | Subproperty of: |
> | Superproperty of: |
> | Quantification: | many to many (0,n:0,n)
> | Scope note: | This symmetric property associates an instance of E53 Place with another instance of E53 Place which shares a part of its border.<br>This property is purely spatial. It does not imply that the phenomena that define, by their extent, places related by P122 borders with have ever shared a respective border at the same time or even coexisted. In particular, this may be the case when the respective common border is formed by a natural feature.<br>This property is not transitive. This property is symmetric
> | Examples: |     • Scotland (E53) borders with England (E53) (Crofton, 2015)
> | In First Order Logic: | P122(x,y) ⇒ E53(x)<br>P122(x,y) ⇒ E53(y)<br>P122(x,y) ⇒ P122(y,x)
> | Properties: |

# P122_est_limitrophe

| | |
|---|---|
| Domaine | `E53_Lieu` |
| Co-domaine | `E53_Lieu` |
| Sous-propriété de |  |
| Super-propriété de | Plusieurs-à-plusieurs (0,n:0,n) |
| Quantification : |  |
| Note d'application : | Cette propriété symétrique associe deux instances `E53_Lieu` qui partagent une frontière.<br>Cette propriété est uniquement spatiale. Cela n’implique pas que le phénomène qui définit cette relation, entraîne que les deux lieux soient limitrophes au même moment ou ce cet état co-éxiste. En particulier, cela peut-être le cas quand quand la limite commune est formé par un limite naturelle.<br>Cette propriété n’est pas transitive. Cette propriété est symétrique. |
| Exemples : | L’Écosse (E53) est limitrophe de l’Angleterre (E53) (Crofton, 2015) |
| Logique du premier ordre: | P122(x,y) ⇒ E53(x)<br>P122(x,y) ⇒ E53(y)<br>P122(x,y) ⇒ P122(y,x) |
| Propriétés: |  |

