> # P89 falls within (contains)

> | | |
> | --- | --- |
> | Domain: | E53 Place
> | Range: | E53 Place
> | Subproperty of: |
> | Superproperty of: |
> | Quantification: | many to many, necessary, dependent (1,n:0,n)
> | Scope note: | This property identifies an instance of E53 Place that falls wholly within the extent of another instance of E53 Place.<br>It addresses spatial containment only and does not imply any relationship between things or phenomena occupying these places.<br>This property is transitive and reflexive.
> | Examples: |     • the area covered by the World Heritage Site of Stonehenge (E53) falls within the area of Salisbury Plain (E53) (Pryor, 2016)
> | In First Order Logic: | P89(x,y) ⇒ E53(x)<br>P89(x,y) ⇒ E53(y)<br>[P89(x,y) ∧ P89(y,z)] ⇒ P89(x,z)<br>P89(x,x)
> | Properties: |

# P89_s’insère_dans_(contient)

| | |
|---|---|
| Domaine | `E53_Lieu` |
| Co-domaine | `E53_Lieu` |
| Sous-propriété de |  |
| Super-propriété de |  |
| Quantification : | plusieurs-à-plusieurs, nécessaire (1,n:0,n) |
| Note d'application : | Cette propriété identifie une instance de `E53 lieu` qui s’insère dans la région couverte par une autre instance de `E53_Lieu`.<br>Cette propriété précise seulement une couverture géographique et n’implique aucune relation entre des objets ou des évènements liés à ces lieux.<br>Cette propriété est transitive et réflective |
| Exemples : | la région couverte par le site de Stonehenge inscrit au Patrimoine mondial (E53) _s’insère dans_ la plaine de Salisbury (E53) (Pryor, 2016) |
| Logique du premier ordre: | P89(x,y) ⇒ E53(x)<br>P89(x,y) ⇒ E53(y)<br>[P89(x,y) ∧ P89(y,z)] ⇒ P89(x,z)<br>P89(x,x) |
| Propriétés: |  |

