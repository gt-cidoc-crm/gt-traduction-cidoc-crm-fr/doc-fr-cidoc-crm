> # P8 took place on or within (witnessed)

> | | |
> | --- | --- |
> | Domain: |		E4 Period
> | Range: |		E18 Physical Thing
> | Subproperty of: |
> | Superproperty of: | 
> | Quantification: |	many to many (0,n:0,n)
> | Scope note: |	This property describes the location of an instance of E4 Period with respect to an instance of E19 Physical Object.<br>P8 took place on or within (witnessed) is a shortcut of the more fully developed path from ‘E4 Period’ through ‘P7 took place at’, ‘E53 Place’, ‘P156i is occupied by’, to ‘E18 Physical Thing’<br>It describes a period that can be located with respect to the space defined by an E19 Physical Object such as a ship or a building. The precise geographical location of the object during the period in question may be unknown or unimportant.<br>For example, the French and German armistice of 22 June 1940 was signed in the same railway carriage as the armistice of 11 November 1918.
> | Examples: | 	<br>the coronation of Queen Elizabeth II (E7) took place on or within Westminster Abbey (E19)
> | In First Order Logic: |<br>P8(x,y) ⊃ E4(x)<br>P8(x,y) ⊃ E18(y)
> | Properties: |

# P8

| | |
|---|---|
| Domaine |  |
| Co-domaine |  |
| Sous-propriété de |  |
| Super-propriété de |  |
| Quantification : |  |
| Note d'application : |  |
| Exemples : |  |
| Logique du premier ordre: |  |
| Propriétés: |  |

