> # P76 has contact point (provides access to)

> | | |
> | --- | --- |
> | Domain: | E39 Actor
> | Range: | E41_Appellation
> | Subproperty of: |
> | Superproperty of: |
> | Quantification: | many to many (0,n:0,n)
> | Scope note: | This property associates an instance of E39 Actor to an instance of E41_Appellation which a communication service uses to direct communications to this actor, such as an e-mail address, fax number, or postal address.
> | Examples: |     • The Research Libraries Group, Inc. (RLG) (E74) has contact point “bl.ric@rlg.org” (E41)
> | In First Order Logic: | P76(x,y) ⇒ E39(x)<br>P76(x,y) ⇒ E41(y)
> | Properties: |

#_P76_a_pour_coordonnées_(permet_de_contacter)

| | |
|---|---|
| Domaine | E39_Acteur·rice·x  |
| Co-domaine | E41_Appellation |
| Sous-propriété de |  |
| Super-propriété de | plus |
| Quantification : | plusieurs-à-plusieurs (0,n:0,n) |
| Note d'application : | Cette propriétés associe une instance de E39_Acteur·ice·x à une instance de E41_Appellation via un service de communication qui permet d’atteindre directement l’acteur·ice·x, comme une adresse électronique, un numéro de fax ou une adresse postale. |
| Exemples : | The Research Libraries Group, Inc. (RLG) (E74) a pour point de coordonnées “ bl.ric@rlg.org” (E41) |
| Logique du premier ordre: | P76(x,y) ⇒ E39(x)<br>P76(x,y) ⇒ E41(y) |
| Propriétés: |  |

