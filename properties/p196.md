> # P196 defines (is defined by)

> | | |
> | --- | --- |
> | Domain: | E18 Physical Thing
> | Range: | E92 Spacetime Volume
> | Subproperty of: |
> | Superproperty of: |
> | Quantification: | one to one, necessary (1,1:0,1)
> | Scope note: | This property associates an instance of E18 Physical Thing with the instance of E92 Spacetime Volume that constitutes the complete trajectory of its geometric extent through spacetime for the whole time of the existence of the instance of E18 Physical Thing.<br>An instance of E18 Physical Thing not only occupies a particular geometric space at each instant of its existence, but in the course of its existence it also forms a trajectory through spacetime, which occupies a real, that is phenomenal, volume in spacetime, i.e., the instance of E92 Spacetime Volume this property associates it with. This real spatiotemporal extent of the instance of E18 Physical Thing is regarded as being unique, in all its details and fuzziness; the identity and existence of the E92 Spacetime Volume depends uniquely on the identity of the instance of E18 Physical Thing, whose existence defines it. It constitutes a phenomenal spacetime volume as defined in CRMgeo (Doerr and Hiebel 2013).<br>Included in this spacetime volume are both the spaces filled by the matter of the physical thing and any inner space that may exist, for instance the interior of a box. Physical things consisting of aggregations of physically unconnected objects, such as a set of chessmen, occupy a finite number of individually contiguous subsets of this spacetime volume equal to the number of objects that constitute the set and that are never connected during its existence.
> | Examples: |  <br>• H.M.S. Temeraire (E22) defines the spacetime volume of H.M.S. Temeraire [it was built, during 1798, in Chatham and destroyed, during 1838, in Rotherhithe] (Willis 2010)<br>• The Saint Titus reliquary (E22) defines the Spacetime Volume of the Saint Titus reliquary (E92) [the reliquary has been produced by the workshop of the Vogiatzis brothers located at Monastiraki, Athens, in 1966 as container for the skull of Saint Titus, which was placed into it at that time and has since then continued to fall within the container's spacetime volume. The reliquary with the skull has been kept in the Saint Titus Church in Heraklion, Crete since 1966] (Fisher&Garvey 2010) (Panotis, 2016)
> | In First Order Logic: | P196(x,y) ⇒ E18(x)<br>P196(x,y) ⇒ E92(y)
> | Properties: |

# P196_définit_(est_défini·e_par)
 
| | |
|---|---|
| Domaine |  E18_Chose_matérielle
| Co-domaine | E92_Volume_spatio-temporel
| Sous-propriété de | 
| Super-propriété de |
| Quantification : | un-à-un, nécessaire (1,1:0,1)
| Note d'application : | Cette propriété associe une instance de E18_Chose_matérielle à une instance de E92_Volume_spatio-temporel qui représente la trajectoire complète de son étendue géométrique à travers l’espace-temps pendant toute la durée de l’existence de l’instance de E18_Chose_matérielle.<br>Une instance de E18_Chose_matérielle occupe non seulement un espace géométrique particulier à chaque instant de son existence, mais elle forme également une trajectoire à travers l’espace-temps. La trajectoire occupe, dans l'espace temps, un volume qui est réel et donc phénomènal, par exemple instance de E92_Volume_spatio-temporel à laquelle cette propriété l'associe. Cette extension spatio-temporelle réelle de l’instance de E18_Chose_matérielle est considérée comme unique dans tous ses détails et son imprécision ; l’identité et l’existence de E92_Volume_spatio-temporel dépendent uniquement de l’existence de E18_Chose_matérielle. Son existence définit celle de E92. Cela constitue un volume spatio-temporel phénoménal tel que défini dans CRMgeo (Doerr et Hiebel 2013).<br>Dans ce volume spatio-temporel sont inclus à la fois les espaces occupés par la matière de la chose physique et à la fois tout éventuel espace intérieur, comme par exemple l’intérieur d’une boîte. Les choses matérielles constituées d’agrégats d’objets physiquement non reliés comme un jeu d’échecs occupent un nombre fini de sous-ensembles, individuellement contigus, de ce volume spatio-temporel. Ce nombre fini est égal au nombre d'objets qui constituent l'ensemble et qui ne sont jamais connectés au cours de son existence.
| Exemples : |• Le bateau H.M.S. Temeraire (E22) définit le volume spatio-temporel du H.M.S. Temeraire [il a été construit en 1798, à Chatham et détruit en 1838, à Rotherhithe] (Willis 2010)<br>• Le reliquaire de saint Tite (E22) définit le volume spatio-temporel du reliquaire de saint Tite (E92) [le reliquaire a été produit par l’atelier des frères Vogiatzis situé dans le quartier de Monastiraki à Athènes en 1966 afin de recueillir le crâne de saint Tite, qui y a été placé à ce moment et qui depuis continue de faire partie du volume spatio-temporel du contenant. Le reliquaire avec le crâne est conservé dans l’église Saint-Tite d’Héraklion en Crète, depuis 1966] (Fisher et Garvey 2010) (Panotis, 2016)
| Logique du premier ordre: | P196(x,y) ⇒ E18(x)<br>P196(x,y) ⇒ E92(y)
| Propriétés: | 
