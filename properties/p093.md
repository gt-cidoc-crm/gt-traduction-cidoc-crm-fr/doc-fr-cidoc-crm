> # P93 took out of existence (was taken out of existence by)

> | | |
> | --- | --- |
> | Domain: | E64 End of Existence
> | Range: | E77 Persistent Item
> | Subproperty of: | E5 Event. P12 occurred in the presence of (was present at): E77 Persistent Item
> | Superproperty of: | E6_Destruction. P13 destroyed (was destroyed by): E18 Physical Thing<br>E68_Dissolution. P99 dissolved (was dissolved by): E74 Group<br>E69 Death. P100 was death of (died in): E21 Person<br>E81_Transformation. P124 transformed (was transformed by): E18 Physical Thing
> | Quantification: | one to many, necessary (1,n:0,1)
> | Scope note: | This property links an instance of E64 End of Existence to the instance E77 Persistent Item taken out of existence by it.<br>In the case of immaterial things, the instance of E64 End of Existence is considered to take place with the destruction of the last physical carrier.<br>This allows an “end” to be attached to any instance of E77 Persistent Item being documented i.e., instances of E70 Thing, E72 Legal Object, E39 Actor, E41_Appellation and E55_Type. For many instances of E77 Persistent Item we know the maximum life-span and can infer, that they must have ended to exist. We assume in that case an instance of E64 End of Existence, which may be as unnoticeable as forgetting the secret knowledge by the last representative of some indigenous nation.
> | Examples: |     • the death of Mozart (E69) took out of existence Mozart (E21) (Deutshc, 1965)
> | In First Order Logic: | P93(x,y) ⇒ E64(x)<br>P93(x,y) ⇒ E77(y)<br>P93(x,y) ⇒ P12(x,y)
> | Properties: |

# P93

| | |
|---|---|
| Domaine |  |
| Co-domaine |  |
| Sous-propriété de |  |
| Super-propriété de |  |
| Quantification : |  |
| Note d'application : |  |
| Exemples : |  |
| Logique du premier ordre: |  |
| Propriétés: |  |

