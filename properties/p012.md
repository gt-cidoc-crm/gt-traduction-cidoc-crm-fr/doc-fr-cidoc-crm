> # P12 occurred in the presence of (was present at)

> | | |
> | --- | --- |
> | Domain: |		E5 Event
> | Range: |		E77 Persistent Item
> | Subproperty of: |
> | Superproperty of: |	E5 Event. P11 had participant (participated in): E39 Actor<br>E7 Activity. P16 used specific object (was used for): E70 Thing<br>E9 Move. P25 moved (moved by): E19 Physical Object<br>E11_Modification. P31 has modified (was modified by): E18 Physical Thing<br>E63 Beginning of Existence. P92 brought into existence (was brought into existence by): E77 Persistent Item<br>E64 End of Existence. P93 took out of existence (was taken out of existence by): E77 Persistent Item<br>E80 Part Removal.P113 removed (was removed by): E18 Physical Thing
> | Quantification: |	many to many, necessary (1,n:0,n)
> | Scope note: |	This property describes the active or passive presence of an E77 Persistent Item in an instance of E5 Event without implying any specific role.<br>It documents known events in which an instance of E77 Persistent Item was present during the course of its life or history. For example, an object may be the desk, now in a museum on which a treaty was signed. The instance of E53 Place and the instance of E52 Time-Span where and when these events happened provide us with constraints about the presence of the related instance E77 Persistent Item in the past. Instances of E90 Symbolic Object, in particular information objects, are physically present in events via at least one of the instances of E18 Physical Thing carrying them. Note, that the human mind can be such a carrier. A precondition for a transfer of information to a person or another new physical carrier is the presence of the respective information object and this person or physical thing in one event.
> | Examples: | Deckchair 42 (E19) was present at The sinking of the Titanic (E5)
> | In First Order Logic: |<br>P12(x,y) ⊃ E5(x)<br>P12(x,y) ⊃ E77(y)
> | Properties: |

# P12_s'est_produit_en_présence_de_(était_présent_lors_de)

| | |
|---|---|
| Domaine | E5_Événement |
| Co-domaine | E77_Entité_persistante |
| Sous-propriété de |  |
| Super-propriété de | E5_Événement. P11_a_eu_pour_participant_(a_participé_à) : E39_Acteur·rice·x<br>E7_Activité. P16_a_mobilisé_l'objet_spécifique_(a_été_mobilisé_pour) : E70_Chose<br>E9_Déplacement. P25_a_déplacé_(a_été_déplacé_par) : E18_Chose_matérielle<br>E63_Début_d’existence. P92_a_fait_exister_(a_commencé_à_exister_par) : E77_Entité_persistante<br>E64_Fin_d’existence. P93_a_mis_fin_à_l’existence_de_(a_cessé_d’exister_du_fait_de) : E77_Entité_persistante<br>E80_Retrait_d’élément. P113_a_retiré_(a_été_retiré_par) : E18_Chose_matérielle |
| Quantification : | plusieurs-à-plusieurs, nécessaire (1,n:0,n) |
| Note d'application : | Cette propriété décrit la présence active ou passive d’une E77_Entité_persistante lors d'une instance de E5_Événement. Cette présence ne signifie pas que l'entité E77 ait joué le moindre rôle pendant l'événement.<br>La propriété documente des événements connus dans l'existence desquels une instance de E77_Entité_persistante a été présente. Par exemple, un objet peut être le bureau, aujourd'hui dans un musée, sur lequel un traité a été signé. Les instances de E53_Lieu et de E52_Laps_de_temps définissent l'endroit et le temps où les événements se sont produits. Par conséquent, elles nous donnent aussi le cadre spatio-temporel de la présence de l'instance de E77_Entité_persistante qui est liée à l'événement. Les instances de E90_Objet_symbolique, en particulier les objets informationnels, sont concrètement présentes à des événements grâce à au moins une des instances de E18_Chose_matérielle qui leur sert de support. Il est à noter que l’esprit humain peut être un de ces supports. Une condition préalable au transfert de l’information vers une autre personne ou un nouveau support matériel est la présence de l’objet informationnel et de cette personne ou chose matérielle à un même événement. |

| Exemples : | La Chaise Longue 42 (E19) a été présente lors du naufrage du Titanic (E5) |
| Logique du premier ordre: | P12(x,y) ⇒ E5(x)<br>P12(x,y) ⇒ E77(y) |

