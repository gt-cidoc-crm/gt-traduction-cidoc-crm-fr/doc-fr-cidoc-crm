> # P132 spatiotemporally overlaps with

> | | |
> | --- | --- |
> | Domain: | E92 Spacetime Volume
> | Range: | E92 Spacetime Volume
> | Subproperty of: |
> | Superproperty of: | E4 Period. P9 consists of (forms part of): E4 Period<br>E92 Spacetime Volume. P10 falls within (contains): E92 Spacetime Volume
> | Quantification: | many to many (0,n:0,n)
> | Scope note: | This symmetric property associates two instances of E92 Spacetime Volume that have some of their extents in common. If only the fuzzy boundaries of the instances of E92 Spacetime Volume overlap, this property cannot be determined from observation alone and therefore should not be applied. However, there may be other forms of justification that the two instances of E92 Spacetime Volume must have some of their extents in common regardless of where and when precisely.<br>If this property holds for two instances of E92 Spacetime Volume then it cannot be the case that P133 is spatiotemporally separated from also holds for the same two instances. Furthermore, there are cases where neither P132 spatiotemporally overlaps with nor P133 is spatiotemporally separated from holds between two instances of E92 Spacetime Volume. This would occur where only an overlap of the fuzzy boundaries of the two instances of E92 Spacetime Volume occurs and no other evidence is available.<br>This property is symmetric
> | Examples: |     • the “Urnfield” period (E4) spatiotemporally overlaps with the “Hallstatt” period (E4) (Gimbutas, 1965)
> | In First Order Logic: | P132(x,y) ⇒ E92(x)<br>P132(x,y) ⇒ E92(y)<br>P132(x,y) ⇒ P132(y,x)<br>P132(x,y) ⇒ ¬P133(x,y)
> | Properties: |

# P132_recouvrement_spatio-temporel

| | |
|---|---|
| Domaine |  |
| Co-domaine |  |
| Sous-propriété de |  |
| Super-propriété de | `E4_Période`, `P9 consiste en ()`: `E4_Période`<br>`E92 Espace spatio-temporel`. `P10 FIXME`: `E92 Espace spatio-temporel` |
| Quantification : | plusieurs-à-plusieurs (0,n:0,n) |
| Note d'application : | Cette propriété symétrique associe deux instances de `E92 Espace spatio-temporel` qui ont des étendues communes. S’il n’y a que des frontières imprécices de ces instances `E92 Espace spatio-temporel`, cette propriétée ne peut pas être déterminé seuleument par observation et est alors non applicable. Cependant, il peut y avoir d’autres justifications qui entraîne que les instances `E92 Espace spatio-temporel` doivent avoir des étendues communes en dépit d’où et de quand précisemment.<br>Si cette propriété |
| Exemples : |  |
| Logique du premier ordre: |  |
| Propriétés: |  |

