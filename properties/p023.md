> # P23 transferred title from (surrendered title through)

> | | |
> | --- | --- |
> | Domain: |		E8_Acquisition
> | Range: |		E39 Actor
> | Subproperty of: | 	E7 Activity. P14 carried out by (performed): E39 Actor
> | Superproperty of: |
> | Quantification: |	many to many (0,n:0,n)
> | Scope note: |	This property identifies the instance(s) of  E39 Actor who relinquish legal ownership as the result of an instance of E8_Acquisition.<br>The property will typically be used to describe a person donating or selling an object to a museum. In reality title is either transferred to or from someone, or both.
> | Examples: | 	<br>acquisition of the Amoudrouz collection by the Geneva Ethnography Museum (E8) transferred title from Heirs of Amoudrouz (E74)
> | In First Order Logic: |<br>P23(x,y) ⊃ E8(x)<br>P23(x,y) ⊃ E39(y)<br>P23 (x,y) ⊃ P14(x,y)
> | Properties: |

# P23_a_transféré_le_titre_de_propriété_de_(a_cédé_le_titre_de_propriété_à)

| | |
| --- | --- |
| Domaine : | E8_Acquisition |
| Co-domaine : | E39_Acteur·rice·x |
| Sous-propriété de : | E7 Activité. P14 a été effectué par (a effectué) : E39_Acteur·rice·x |
| Super-propriété de : | |
| Quantification : | plusieurs-à-plusieurs (0,n:0,n) |
| Exemples : | acquisition le la collection Amoudrouz par le musée d’ethnographie de Genève (E8) a transféré le titre de propriété des héritiers de Amoudrouz (E74) |
| Logique du premier ordre : |<br> P23(x,y) ⇒  E8(x)<br> P23(x,y) ⇒  E39(y) <br> P23 (x,y) ⇒  P14(x,y) |
| Propriétés: | |

