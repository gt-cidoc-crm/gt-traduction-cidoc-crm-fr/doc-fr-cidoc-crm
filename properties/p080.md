> # P80 end is qualified by

> | | |
> | --- | --- |
> | Domain: | E52 Time-Span
> | Range: | E62 String
> | Subproperty of: | E1 CRM Entity. P3 has note: E62 String
> | Superproperty of: |
> | Quantification: | many to one (0,1:0,n)
> | Scope note: | This property associates an instance of E52 Time-Span with a note detailing the scholarly or scientific opinions and justifications about the end of this time-span concerning certainty, precision, sources etc. This property may also be used to describe arguments constraining possible dates and to distinguish reasons for alternative dates.
> | Examples: |     • The time-span of the Holocene (E52) *end is qualified by* “still ongoing” (E62). (Walker et al, 2009)
> | In First Order Logic: | P80(x,y) ⇒ E52(x)<br>P80(x,y) ⇒ E62(y)<br>P80(x,y) ⇒ P3(x,y)
> | Properties: |

# P80_a_sa_fin_qualifiée_par

| | |
|---|---|
| Domaine | `E52_Laps_de_temps`
| Co-domaine |  `E62_Chaîne_de_caractères`
| Sous-propriété de | `E1 CRM Entity`. `P3_a_pour_note` : `E62_Chaîne_de_caractères`
| Super-propriété de |  |
| Quantification : | plusieurs-à-un (0,1:0,n)
| Note d'application : | Cette propriété associe une instance de `E52_Laps_de_temps` avec une note qui détaille les opinions et justifications académiques ou scientifiques concernant la fin de ce laps de temps en ce qui concerne la certitude, la précision, les sources etc. Cette propriété peut également être utilisée pour élaborer des arguments qui contraignent les dates possibles ou pour donner différentes explications à des dates alternatives. 
| Exemples : | La fin du Laps de temps de l’Holocène (E52) est qualifiée comme «toujours en cours» (Walker et al, 2009).
| Logique du premier ordre: | P80(x,y) ⇒ E52(x)<br>P80(x,y) ⇒ E62(y)<br>P80(x,y) ⇒ P3(x,y)
| Propriétés: |  |

