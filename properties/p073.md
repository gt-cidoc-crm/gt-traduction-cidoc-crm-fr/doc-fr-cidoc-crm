> # P73 has translation (is translation of)

> | | |
> | --- | --- |
> | Domain: | E33 Linguistic Object
> | Range: | E33 Linguistic Object
> | Subproperty of: | E70 Thing. P130i features are also found on: E70 Thing
> | Superproperty of: |
> | Quantification: | many to many (0,n:0,n)
> | Scope note: | This property links an instance of E33 Linguistic Object (A), to another instance of E33 Linguistic Object (B) which is the translation of A.<br>When an instance of E33 Linguistic Object is translated into a new language a new instance of E33 Linguistic Object is created, despite the translation being conceptually similar to the source.<br>This property is non-symmetric.
> | Examples: |     • “Les Baigneurs” (E33) has translation “The Bathers” (E33) (Surenne, 1840)
> | In First Order Logic: | P73(x,y) ⇒ E33(x)<br>P73(x,y) ⇒ E33(y)<br>P73(x,y) ⇒ P130i(x,y)<br>P73(x,y) ⇒ ¬P73(y,x)
> | Properties: |

# P73_a_pour_traduction_(est_la_traduction_de)

| | |
|---|---|
| Domaine | E33_Objet_linguistique |
| Co-domaine |  E33_Objet_linguistique |
| Sous-propriété de | E70_Chose. P130i_a_des_caractéristiques_également_présentes_sur: E70_Chose |
| Super-propriété de |  |
| Quantification : |  plusieurs-à-plusieurs (0,n:0,n) |
| Note d'application : | Cette propriété lie une instance de E33_Objet_linguistique (A) à une autre instance de E33_Objet_linguistique (B) qui est la traduction de (A).<br>Quand une instance de E33_Objet_linguistique est traduit dans une autre langue, une nouvelle instance de E33_Objet_linguistique est créée, indépendamment du fait que, conceptuellement parlant, la traduction est similaire au texte originel.<br>Cette propriété est asymétrique. |
| Exemples : | “Les Baigneurs” (E33) a pour traduction “The Bathers” (E33) (Spiers & Surenne, 1854) |
| Logique du premier ordre: | P73(x,y) ⇒ E33(x)<br>P73(x,y) ⇒ E33(y)<br>P73(x,y) ⇒ P130i(x,y)<br>P73(x,y) ⇒ ¬P73(y,x) |
| Propriétés: |  |

