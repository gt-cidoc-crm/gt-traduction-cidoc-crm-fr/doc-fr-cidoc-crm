> # P137 exemplifies (is exemplified by)

> | | |
> | --- | --- |
> | Domain: | E1 CRM Entity 
> | Range: | E55_Type 
> | Subproperty of: | E1 CRM Entity. P2 has type (is type of):E55_Type
> | Superproperty of: |
> | Quantification: | many to many (0,n:0,n)
> | Scope note: | This property associates an instance of E1 CRM Entity with an instance of E55_Type for which it has been declared to be a particularly characteristic example.<br>The P137.1 in the taxonomic role property of P137 exemplifies (is exemplified by) allows differentiation of taxonomic roles. The taxonomic role renders the specific relationship of this example to the type, such as "prototypical", "archetypical", "lectotype", etc. The taxonomic role "lectotype" is not associated with the instance of E83 Type Creation itself, but selected in a later phase.
> | Examples: |     • Object BM000098044 of the Clayton Herbarium (E20) exemplifies Spigelia marilandica (L.) L. (E55) in the taxonomic role lectotype (Savage, 1945)
> | In First Order Logic: | P137(x,y) ⇒ E1(x)<br>P137(x,y) ⇒ E55(y)<br>P137(x,y,z) ⇒ [P137(x,y) ∧ E55(z)]<br>P137(x,y) ⇒ P2(x,y)
> | Properties: | P137.1 in the taxonomic role: E55_Type

# P137

| | |
|---|---|
| Domaine |  |
| Co-domaine |  |
| Sous-propriété de |  |
| Super-propriété de |  |
| Quantification : |  |
| Note d'application : |  |
| Exemples : |  |
| Logique du premier ordre: |  |
| Propriétés: |  |

