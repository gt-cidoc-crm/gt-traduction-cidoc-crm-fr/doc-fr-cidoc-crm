> # P147 curated (was curated by)

> | | |
> | --- | --- |
> | Domain: | E87 Curation Activity
> | Range: | E78 Curated Holding
> | Subproperty of: |
> | Superproperty of: |
> | Quantification: | many to many, necessary (1,n:0,n)
> | Scope note: | This property associates an instance of E87 Curation Activity with the instance of E78 Curated Holding with that is subject of that curation activity following some implicit or explicit curation plan.
> | Examples: |     • The curation activities of the Benaki Museum (E87) curated the acquisition of dolls and games of urban and folk manufacture dating from the 17th to the 20th century, from England, France and Germany for the “Toys, Games and Childhood Collection (E78) of the Museum (Benaki Museum, 2016)<br>• The curation activities of the Historical Museum of Crete, Heraklion, Crete (E87) curated the development of the permanent Numismatic Collection (E78) (Historical Museum of Crete, 2005-2020)<br>• The curation activities of Mikael Heggelund Foslie (E87)  curated the Mikael Heggelund Foslie’s coralline red algae Herbarium (Woelkerling, 2005)
> | In First Order Logic: |<br>P147(x,y) ⇒ E87(x)<br>P147(x,y) ⇒ E78(y)
> | Properties: |

# P147

| | |
|---|---|
| Domaine |  |
| Co-domaine |  |
| Sous-propriété de |  |
| Super-propriété de |  |
| Quantification : |  |
| Note d'application : |  |
| Exemples : |  |
| Logique du premier ordre: |  |
| Propriétés: |  |

