> # P75 possesses (is possessed by)

> | | |
> | --- | --- |
> | Domain: | E39 Actor
> | Range: | E30 Right
> | Subproperty of: |
> | Superproperty of: |
> | Quantification: | many to many (0,n:0,n)
> | Scope note: | This property associates an instance of E39 Actor to an instance of E30 Right over which the actor holds or has held a legal claim. 
> | Examples: |     • Michael Jackson (E21) possesses Intellectual property rights on the Beatles’ back catalogue (E30) (Raga, 2016)
> | In First Order Logic: | P75(x,y) ⇒ E39(x)<br>P75(x,y) ⇒ E30(y)
> | Properties: |

# P75_est_détenteur_de_(est_détenu_par)

| | |
|---|---|
| Domaine | E39_Acteur·rice·x |
| Co-domaine | E30_Droit |
| Sous-propriété de |  |
| Super-propriété de |  |
| Quantification : | plusieurs-à-plusieurs |
| Note d’application : | Cette propriété associe une instance de E39_Acteur·rice·x à une instance de E30_Droit sur laquelle l’acteur·rice·x détient ou a détenu un titre officiel. |
| Exemples : | Michael Jackson (E21) est détenteur des droits de propriété intellectuelle (E30) sur le fond de catalogue des Beatles (Raga, 2016) |
| Logique du premier ordre: | P75(x,y) ⇒ E39(x)<br>P75(x,y) ⇒ E30(y) |
| Propriétés: |  |

