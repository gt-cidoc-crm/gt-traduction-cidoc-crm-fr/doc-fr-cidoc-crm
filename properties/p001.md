> # P1 is identified by (identifies)

> | | |
> | --- | --- |
> | Domain: |		E1 CRM Entity
> | Range: |		E41_Appellation
> | Superproperty of: |E1 CRM Entity. P48 has preferred identifier (is preferred identifier of): E42 Identifier<br>E71 Human-Made Thing. P102 has title (is title of): E35 Title<br>E53 Place. P168 place is defined by (defines place): E94 Space Primitive<br>E95 Spacetime Primitive. P169i spacetime volume is defined by: E92 Spacetime Volume<br>E61 Time Primitive. P170i time is defined by: E52 Time Span
> | Quantification: |	many to many (0,n:0,n)
> | Scope note: |	This property describes the naming or identification of any real-world item by a name or any other identifier.<br>This property is intended for identifiers in general use, which form part of the world the model intends to describe, and not merely for internal database identifiers which are specific to a technical system, unless these latter also have a more general use outside the technical context. This property includes in particular identification by mathematical expressions such as coordinate systems used for the identification of instances of E53 Place. The property does not reveal anything about when, where and by whom this identifier was used. A more detailed representation can be made using the fully developed (i.e. indirect) path through E15 Identifier Assignment.<br>This property is a shortcut for the path from E1 CRM Entity through ‘P140i was attributed by’, E15 Identifier Assignment, ‘P37 assigned’to E42 Identifier.<br>It is also a shortcut for the path from E1 CRM Entity through P1 is identified by, E41_Appellation, P139 has alternative form to E41_Appellation.
> | Examples: | 	<br>The capital of Italy (E53) is identified by “Rome”. (Leach, 2017) (E41)<br>Text 25014–32 (E33) is identified by “The Decline and Fall of the Roman Empire” (E35). (Gibbon, 2013)
> | In First Order Logic: |<br>P1(x,y) ⇒ E1(x)<br>P1(x,y) ⇒ E41(y)<br>P1(x,y) ⇐ (∃z) [E15(z)^P140i(x,z)^P37(z,y)]<br>P1(x,y) ⇐ (∃z) [E41(z)^P1(x,z)^P139(z,y)] 
> | Properties: |

# P1_est_identifié·e_par_(identifie)

| | |
|---|---|
| Domaine |  E1_Entité_CRM |
| Co-domaine | E41_Appellation |
| Super-propriété de | E1_Entité_CRM. P48_a_pour_identifiant_préférentiel_(est_l’identifiant_préférentiel_de) : E42_Identifiant<br>E71_Chose_anthropique. P102 a pour titre (est le titre de) : E35_Titre |
| Quantification : | plusieurs-à-plusieurs (0,n:0,n) |
| Note d'application : | Cette propriété décrit la dénomination ou l'identification de tout objet du monde réel par un nom ou tout autre identifiant.<br>Cette propriété est destinée aux identifiants en général, qui font partie du monde que le modèle entend décrire, et pas seulement aux identifiants internes des bases de données qui sont spécifiques à un système technique, à moins que ces derniers n'aient également un usage plus général en dehors du contexte technique. Cette propriété comprend notamment l'identification par des expressions mathématiques telles que les systèmes de coordonnées utilisés pour l'identification des instances de E53_Lieu. Cette propriété n'indique rien sur le moment, le lieu et les personnes qui ont utilisé cet identifiant. Une représentation plus détaillée peut être faite en utilisant le chemin entièrement développé (c'est-à-dire indirect) par E15_Attribution_d’identifiant.<br>P1_est_identifié·e_par_(identifie), est un raccourci pour le chemin de "E1_Entité_CRM" à "P140i a reçu un attribut par me biais de", "E15_Attribution_d’identifiant", "P37_a_attribué_(a_été_attribué_par)", "E42_Identifiant". |
| Exemples : | la capitale de l'Italie (E53) est identifiée par "Rome" (E41)<br>le texte 25014-32 (E33) est identifié par "Le déclin et la chute de l'Empire romain" (E35) |
| Logique du premier ordre : |<br>P1(x,y) ⇒ E1(x)<br>P1(x,y) ⇒ E41(y)<br>P1(x,y) ⇐ (∃z) [E15(z)^P140i(x,z)^P37(z,y)]<br>P1(x,y) ⇐ (∃z) [E41(z)^P1(x,z)^P139(z,y)] |
| Propriétés: |  |

