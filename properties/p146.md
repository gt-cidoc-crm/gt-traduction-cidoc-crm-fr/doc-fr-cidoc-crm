> # P146 separated from (lost member by)

> | | |
> | --- | --- |
> | Domain: | E86 Leaving
> | Range: | E74 Group
> | Subproperty of: | E5 Event. P11 had participant (participated in): E39 Actor 
> | Superproperty of: |
> | Quantification: | many to many, necessary (1,n:0,n)
> | Scope note: | This property identifies the instance of E74 Group an instance of E39 Actor leaves through an instance of E86 Leaving.<br>Although a Leaving activity normally concerns only one instance of E74 Group, it is possible to imagine circumstances under which leaving one E74 Group implies leaving another E74 Group as well.
> | Examples: |     • The end of Sir Isaac Newton’s duty as Member of Parliament for the University of Cambridge to the Convention Parliament in 1702 separated from the Convention Parliament (Iliffe, 2013)<br>• George Washington’s leaving office in 1797 separated from the office of President of the United States (Unger, 2015)<br>• The implementation of the treaty regulating the termination of Greenland membership in EU between EU, Denmark and Greenland February 1. 1985 separated from EU (E74)
> | In First Order Logic: | P146(x,y) ⇒ E86(x)<br>P146(x,y) ⇒ E74(y)<br>P146(x,y) ⇒ P11(x,y)
> | Properties: |

# P146

| | |
|---|---|
| Domaine |  |
| Co-domaine |  |
| Sous-propriété de |  |
| Super-propriété de |  |
| Quantification : |  |
| Note d'application : |  |
| Exemples : |  |
| Logique du premier ordre: |  |
| Propriétés: |  |

