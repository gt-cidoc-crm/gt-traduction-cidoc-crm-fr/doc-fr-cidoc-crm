> # P107 has current or former member (is current or former member of)
> | | |
> | --- | --- |
> | Domain: | E74 Group
> | Range: | E39 Actor
> | Subproperty of: |
> | Superproperty of: |
> | Quantification: | many to many (0,n:0,n)
> | Scope note: | This property associates an instance of E74 Group with an instance of E39 Actor that is or has been a member thereof.<br>Instances of E74 Group and E21 Person, may all be members of instances of E74 Group.An instance of E74 Group may be founded initially without any member.<br>This property is a shortcut of the more fully developed path E74 Group, P144i gained member by, E85 Joining, P143 joined, E39 Actor.<br>The property P107.1 kind of member can be used to specify the type of membership or the role the member has in the group. 
> | Examples: |     • Moholy Nagy (E21) is current or former member of Bauhaus (E74) (Moholy Nagy, 2005)<br>• National Museum of Science and Industry (E74) has current or former member The National Railway Museum (E74) (Rolt, 1971)<br>• The married couple Queen Elisabeth and Prince Phillip (E74) has current or former member Prince Phillip (E21) with P107.1 kind of member husband (E55) (Brandreth, 2004)
> | In First Order Logic: | P107(x,y) ⇒ E74(x)<br>P107(x,y) ⇒ E39(y)<br>P107(x,y,z) ⇒ [P107(x,y) ∧ E55(z)]<br>P107(x,y) ⇐ (∃z) [E85(z) ˄ P144i(x,z) ˄ P143(z,y)]
> | Properties: | P107.1 kind of member: E55_Type

# P107_a_pour_membre_actuel_ou_ancien_(est_membre_actuel_ou_ancien)

| | |
|---|---|
| Domaine | E74_Groupe |
| Co-domaine | E39_Acteur·rice·x |
| Sous-propriété de |  |
| Super-propriété de |  |
| Quantification : | plusieurs-à-plusieurs (0,n:0,n) |
| Note d'application : | Cette propriété associe une instance de E74_Groupe à une instance de E39_Acteur·rice·x qui en est ou en a été membre. Les instances de E74_Groupe et de E21_Personne peuvent toutes être membres d'instances de E74_Groupe. Une instance de E74_Groupe peut être fondée initialement sans aucun membre. Cette propriété est un raccourci du chemin plus développé E74_Groupe, P144i_a_gagné_un_membre_du_fait_de, E85_Intégration, P143_a_fait_entrer, E39_Acteur·rice·x. La propriété P107.1_type_de_membre peut être utilisée pour spécifier le type de membre ou le rôle du membre dans le groupe. |
| Exemples : | Moholy Nagy (E21) est membre actuel ou ancien du Bauhaus (E74) (Moholy Nagy, 2012)<br>Le Musée National de la Science et de l’Industrie (E74) a pour membre actuel ou ancien le Musée National du Rail (E74) (Rolt, 1971)<br>le couple constitué de la reine Elisabeth et du prince Phillip (E74) a comme membre le prince Phillip (E21) avec P107.1 le type de membre “époux” (E55) (Brandreth, 2004) |
| Logique du premier ordre: | P107(x,y) ⇒ E74(x)<br>P107(x,y) ⇒ E39(y)<br>P107(x,y,z) ⇒ [P107(x,y) ∧ E55(z)]<br>P107(x,y) ⇐ (∃z) [E85(z) ˄ P144i(x,z) ˄ P143(z,y)] |
| Propriétés: | P107.1_type_de_membre : E55_Type |

