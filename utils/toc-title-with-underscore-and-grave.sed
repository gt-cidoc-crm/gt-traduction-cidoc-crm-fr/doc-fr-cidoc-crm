s/E1 CRM Entité/E1_Entité_CRM/g
s/E1 Entité CRM/E1_Entité_CRM/g
s/E1 Entité/E1_Entité_CRM/g
s/E2 Entité temporelle/E2_Entité_temporelle/g
s/E2 Entité Temporelle/E2_Entité_temporelle/g
s/E3 Etat/E3_État_matériel/g
s/E3_État matériel/E3_État_matériel/g
s/E3 État matériel/E3_État_matériel/g
s/E3 État Matériel/E3_État_matériel/g
s/E3_État_physique/E3_État_matériel/g
s/E4 Période/E4_Période/g
s/E5 Événement/E5_Événement/g
s/E6 Destruction/E6_Destruction/g
s/E7 Activité/E7_Activité/g
s/E8 Acquisition/E8_Acquisition/g
s/E9 Déplacement/E9_Déplacement/g
s/E10 Transfert de la responsabilité/E10_Transfert_de_la_conservationE10_Transfert_de_la_responsabilité/g
s/E10 Transfert de charge\/de la garde/E10_Transfert_de_la_conservation/g
s/E10 Transfert de la conservation/E10_Transfert_de_la_conservation/g
s/E11 Modification/E11_Modification/g
s/E12 Production/E12_Production/g
s/E13 Affectation d'attribut/E13_Affectation_d’un_attribut/g
s/E13 Affectation d’un attribut/E13_Affectation_d’un_attribut/g
s/E13 Affectation d’attribut/E13_Affectation_d’un_attribut/g
s/E14 Évaluation d’état matériel/E14_Évaluation_d’état_matériel/g
s/E14 Évaluation d'état matériel/E14_Évaluation_d’état_matériel/g
s/E14_Évaluation_d’état_matériel/E14_Évaluation_d’état_physique/g
s/E15 Attribution d’identifiant/E15_Attribution_d’identifiant/g
s/E15 Attribution d'identifiant/E15_Attribution_d’identifiant/g
s/E15 Attribution d'assignement/E15_Attribution_d’identifiant/g
s/E16 Mesurage/E16_Mesurage/g
s/E17 Attribution de type/E17_Attribution_de_type/g
s/E18 une chose matérielle/E18_Chose_matérielle/g
s/E18 Chose matérielle/E18_Chose_matérielle/g
s/E18 Chose Matérielle/E18_Chose_matérielle/g
s/E18 Chose physique/E18_Chose_matérielle/g
s/E18 Chose Physique/E18_Chose_matérielle/g
s/E19 Objet matériel/E19_Objet_matériel/g
s/E19 Objet Matériel/E19_Objet_matériel/g
s/E19 Objet Physique/E19_Objet_matériel/g
s/E19 Objets matériels/E19_Objet_matériel/g
s/E20 Objet biologique/E20_Objet_biologique/g
s/E20 Objet Biologique/E20_Objet_biologique/g
s/E21 Personne/E21_Personne/g
s/E22 Objet fabriqué/E22_Objet_fabriqué/g
s/E22 Objet Fabriqué/E22_Objet_fabriqué/g
s/E22_Objet_fabriqué/E22_Objet_anthropique/g
s/E24 Chose fabriquée/E24_Chose_matérielle_fabriquée/g
s/E24 Chose Matérielle Fabriquée/E24_Chose_matérielle_fabriquée/g
s/E24 Chose matérielle fabriquée/E24_Chose_matérielle_fabriquée/g
s/E24_Chose_matérielle_fabriquée/E24_Chose_matérielle_anthropique/g
s/P109 a pour conservateur actuel ou antérieur (est conservateur actuel ou antérieur de)E25 Caractéristique fabriquée/E25_Caractéristique_fabriquée/g
s/E25 Trait caractéristique fabriqué/E25_Caractéristique_fabriquée/g
s/E25 Caractéristique Fabriquée/E25_Caractéristique_fabriquée/g
s/E25_Caractéristique_fabriquée/E25_Caractéristique_anthropique/g
s/E26 Caractéristique physique/E26_Caractéristique_matérielle/g
s/E26 Caractéristique matérielle/E26_Caractéristique_matérielle/g
s/E26 Caractéristique Matérielle/E26_Caractéristique_matérielle/g
s/E27 Site/E27_Site/g
s/E28 Objet conceptuel/E28_Objet_conceptuel/g
s/E28 Objet Conceptuel/E28_Objet_conceptuel/g
s/E29 Projet ou Procédure/E29_Conception_ou_Procédure/g
s/E29 Projet ou procédure/E29_Conception_ou_Procédure/g
s/E29_Conception_ou_Planification/E29_Conception_ou_Procédure/g
s/E30 Droit/E30_Droit/g
s/E31 Document/E31_Document/g
s/E32 Document d’Autorité/E32_Document_d’autorité/g
s/E32 Document d'Autorité/E32_Document_d’autorité/g
s/E32 Document d’autorité/E32_Document_d’autorité/g
s/E32 Document d'autorité/E32_Document_d’autorité/g
s/E33 Objet linguistique/E33_Objet_linguistique/g
s/E33 Objet Linguistique/E33_Objet_linguistique/g
s/E34 Inscription/E34_Inscription/g
s/E35 Titre/E35_Titre/g
s/E36 Élément visuel/E36_Élément_visuel/g
s/E36 Élément Visuel/E36_Élément_visuel/g
s/E36 Elément visuel/E36_Élément_visuel/g
s/E36 Elément Visuel/E36_Élément_visuel/g
s/E36 Item visuel/E36_Élément_visuel/g
s/E36 Entité visuelle/E36_Élément_visuel/g
s/E37 Marque/E37_Marque/g
s/E39 Acteur·rice·x/E39_Acteur·rice·x/g
s/E39 Acteur\([ ,\.]\)\{0,1\}/E39_Acteur·rice·x\1/g
s/E39_Acteur·rice·x·ice·x/E39_Acteur·rice·x/g
s/E41 Appellation/E41_Appellation/g
s/E42 Identifiant/E42_Identifiant/g
s/E52 Laps de temps/E52_Laps_de_temps/g
s/E52 Laps de Temps/E52_Laps_de_temps/g
s/E52 Interval Temporel/E52_Laps_de_temps/g
s/E53 Lieu/E53_Lieu/g
s/E54 Dimension/E54_Dimension/g
s/E55 Type/E55_Type/g
s/E56 Langue/E56_Langue/g
s/E57 Matériau/E57_Matériau/g
s/E58 Unité de mesure/E58_Unité_de_mesure/g
s/E59 Valeur primitive/E59_Valeur_primitive/g
s/E59 Valeur Primitive/E59_Valeur_primitive/g
s/E60 Nombre/E60_Nombre/g
s/E61 Primitive de temps/E61_Primitive_de_temps/g
s/E62 Chaîne de caractères/E62_Chaîne_de_caractères/g
s/E62 Chaîne de Caractères/E62_Chaîne_de_caractères/g
s/E63 Début d’existence/E63_Début_d’existence/g
s/E63 Début d'existence/E63_Début_d’existence/g
s/E63 Début d'Existence/E63_Début_d’existence/g
s/E64_Fin_de_l'existence/E64_Fin_d’existence/g
s/E64 Fin d’existence/E64_Fin_d’existence/g
s/E64 Fin d'existence/E64_Fin_d’existence/g
s/E64 Fin d’Existence/E64_Fin_d’existence/g
s/E65 Création/E65_Création/g
s/E66 Formation/E66_Formation/g
s/E67 Naissance/E67_Naissance/g
s/E68 Dissolution/E68_Dissolution/g
s/E69 Mort/E69_Mort/g
s/E70 Chose/E70_Chose/g
s/E71 Chose élaborée par l’humain/E71_Chose_élaborée_par_l’humain/g
s/E71 Chose élaborée par l'humain/E71_Chose_élaborée_par_l’humain/g
s/E71 Chose fabriquée/E71_Chose_élaborée_par_l’humain/g
s/E71 Chose Fabriquée/E71_Chose_élaborée_par_l’humain/g
s/E71_Chose_élaborée_par_l’humain/E71_Chose_anthropique/g
s/E72 Objet juridique/E72_Objet_juridique/g
s/E73 Objet Informationnel/E73_Objet_informationnel/g
s/E74 Groupe/E74_Groupe/g
s/E77 Entité persistante/E77_Entité_persistante/g
s/E77 Entité Persistante/E77_Entité_persistante/g
s/E78 Collection/E78_Collection/g
s/E79 Ajout d’élément/E79_Ajout_d’élément/g
s/E79 Ajout d'élément/E79_Ajout_d’élément/g
s/E79 Ajout d'Élément/E79_Ajout_d’élément/g
s/E79 Ajout d’Élément/E79_Ajout_d’élément/g
s/E79 Ajout d’élément/E79_Ajout_d’élément/g
s/E79_Ajout_d’élémént/E79_Ajout_d’élément/g
s/E80 Retrait d'élément/E80_Retrait_d’élément/g
s/E80 Retrait d’Élément/E80_Retrait_d’élément/g
s/E81 Transformation/E81_Transformation/g
s/E83 Création de type/E83_Création_de_type/g
s/E85 Intégration/E85_Intégration/g
s/E86 Départ/E86_Départ/g
s/E87 Activité curatoriale/E87_Activité_curatoriale/g
s/E87 Activité Curatoriale/E87_Activité_curatoriale/g
s/E87 Activité de conservation/E87_Activité_curatoriale/g
s/E89 Objet propositionnel/E89_Objet_propositionnel/g
s/E89 Objet Propositionnel/E89_Objet_propositionnel/g
s/E90 Objet symbolique/E90_Objet_symbolique/g
s/E90 Objet Symbolique/E90_Objet_symbolique/g
s/E92 Volume spatio-temporel/E92_Volume_spatio-temporel/g
s/E92 Volume spatio-temporel/E92_Volume_spatio-temporel/g
s/E92 Volume Spatio-temporel/E92_Volume_spatio-temporel/g
s/E94 Référence spatiale/E94_Référence_spatiale/g
s/E94_Référence_spatiale/E94_Primitive_spatiale/g
s/E94 Espace primitif/E94_Primitive_spatiale/g
s/E96 Achat/E96_Achat/g
s/E98 Devise/E98_Devise/g
s/E99 type de produit/E99_Type_de_produit/g
s/E99 Type de produit/E99_Type_de_produit/g
#------------------------------- Properties ----------------------
s/P1 est identifié par (identifie)/P1_est_identifié_par_(identifie)/g
s/P1_est_identifié_par_(identifie)/P1_est_identifié·e_par_(identifie)/g
s/P2 a pour type (est de type)/P2_a_pour_type_(est_de_type)/g
s/P2_a_pour_type_(est_de_type)/P2_a_pour_type_(est_le_type_de)/g
s/P2 est de type/P2_a_pour_type_(est_le_type_de)/g
s/P3 a pour note/P3_a_pour_note/g
s/P3.1 a pour type/P3.1_a_pour_type/g
s/P4 a pour laps de temps (est laps de temps de)/P4_a_pour_laps_de_temps_(est_laps_de_temps_de)/g
s/P4 a pour laps de temps (est le laps de temps de)/P4_a_pour_laps_de_temps_(est_laps_de_temps_de)/g
s/P7 a eu lieu en (a été témoin de)/P7_a_eu_lieu_en_(a_été_témoin_de)/g
s/P7 a eu lieu dans (a été témoin de)/P7_a_eu_lieu_à_(a_été_témoin_de)/g
s/P7_a_eu_lieu_en_(a_été_témoin_de)/P7_a_eu_lieu_à_(a_été_témoin_de)/g
s/P11 a eu pour participant (a participé à)/P11_a_eu_pour_participant_(a_participé_à)/g
s/P12 s'est produit en présence de (était présent lors de)/P12_s'est_produit_en_présence_de_(était_présent_lors_de)/g
s/P14 a été effectué par (a effectué)/P14_a_été_effectué_par_(a_effectué)/g
s/P15 a été influencé·e par (a influencé)/P15_a_été_influencé·e_par_(a_influencé)/g
s/P16 a mobilisé l'objet spécifique (a été mobilisé pour)/P16_a_mobilisé_l'objet_spécifique_(a_été_mobilisé_pour)/g
s/P16 a utilisé l’objet spécifique (a été utilisé pour)/P16_a_mobilisé_l'objet_spécifique_(a_été_mobilisé_pour)/g
s/P17 a été motivée par (a motivé)/P17_a_été_motivée_par_(a_motivé)/g
s/P19 a été l'usage prévu de (a été fabriquée pour)/P19_a_été_l'usage_prévu_de_(a_été_fabriquée_pour)/g
s/P20 a eu pour finalité spécifique (a été finalité de)/P20_a_eu_pour_finalité_spécifique_(a_été_finalité_de)/g
s/P21 a eu pour finalité générale (a été finalité de)/P21_a_eu_pour_finalité_générale_(a_été_finalité_de)/g
s/P22 a transféré le titre de propriété à (a acquis le titre de propriété par)/P22_a_transféré_le_titre_de_propriété_à_(a_acquis_le_titre_de_propriété_par)/g
s/P23 a transféré le titre de propriété de (a cédé le titre de propriété à)/P23_a_transféré_le_titre_de_propriété_de_(a_cédé_le_titre_de_propriété_à)/g
s/P24 a transféré le titre de propriété du (a changé de propriétaire par)/P24_a_transféré_le_titre_de_propriété_du_(a_changé_de_propriétaire_par)/g
s/P25 a déplacé (a été déplacé par)/P25_a_déplacé_(a_été_déplacé_par)/g
s/P25i a déplacé/P25i_a_été_déplacé·e_par/g
s/P25i a été déplacé par/P25i_a_été_déplacé·e_par/g
s/P26 a déplacé vers (a été destination de)/P26_a_déplacé_vers_(a_été_destination_de)/g
s/P26 a déplacé vers/P26_a_déplacé_vers/g
s/P27 a déplacé de (a été point de départ de)/P27_a_déplacé_de_(a_été_point_de_départ_de)/g
s/P27 a déplacé de/P27_a_déplacé_de/g
s/P28 a retiré la responsabilité (a perdu la responsabilité du fait de)/P28_a_retiré_la_conservation_à_(a_perdu_la_conservation_du_fait_de)/g
s/P28 a retiré la responsabilité à (a perdu la responsabilité du fait de)/P28_a_retiré_la_conservation_à_(a_perdu_la_conservation_du_fait_de)/g
s/P29 a confié la responsabilité à (a reçu la responsabilité par)/P29_a_confié_la_conservation_à_(a_reçu_la_conservation_de)/g
s/P30 responsabilité transférée de (la responsabilité transférée par)/P30_conservation_transférée_de_(la_conservation_transférée_à)/g
s/P30i la responsabilité transférée par/P30i_la_conservation_transférée_à/g
s/P32 a mobilisé comme technique générique (a été la technique mise en oeuvre dans)/P32_a_mobilisé_comme_technique_générique_(a_été_la_technique_mise_en_oeuvre_dans)/g
s/P33 a mobilisé comme technique spécifique (a été mise en œuvre dans)/P33_a_mobilisé_comme_technique_spécifique_(a_été_mise_en_œuvre_dans)/g
s/P34 a concerné (a été évalué par le biais de)/P34_a_concerné_(a_été_évalué_par_le_biais_de)/g
s/P34_a_concerné_(a_été_évalué_par_le_biais_de)/P34_a_concerné_(a_été_évalué_par)/g
s/P35 a identifié (a été identifié par)/P35_a_identifié_(a_été_identifié_par)/g
s/P37 a attribué (a été attribué par)/P37_a_attribué_(a_été_attribué_par)/g
s/P37 a attribué/P37_a_attribué_(a_été_attribué_par)/g
s/P38 a retiré l’attribution de (a été retiré par le biais de)/P38_a_retiré_l’attribution_de_(a_été_retiré_par_le_biais_de)/g
s/P39 a mesuré (a été mesuré par)/P39_a_mesuré_(a_été_mesuré_par)/g
s/P40 a relevé comme dimension (a été relevé lors de)/P40_a_relevé_comme_dimension_(a_été_relevé_lors_de)/g
s/P43 a pour dimension (est dimension de)/P43_a_pour_dimension_(est_dimension_de)/g
s/P44 a pour état matériel (est état matériel de)/P44_a_pour_état_matériel_(est_état_matériel_de)/g
s/P44_a_pour_état_matériel_(est_état_matériel_de)/P44_a_pour_état_physique_(est_état_physique_de)/g
s/P45 consiste en (inclut)/P45_est_constitué·e_de_(est_incorporé·e_dans)/g
s/P45 est en(est présent dans)/P45_est_constitué·e_de_(est_incorporé·e_dans)/g
s/P45_consiste_en_(inclut)/P45_est_constitué·e_de_(est_incorporé·e_dans)/g
s/P45 est en (est présent dans)/P45_est_constitué·e_de_(est_incorporé·e_dans)/g
s/P46 est composé de (fait partie de)/P46_est_composé·e_de_(fait_partie_de)/g
s/P46_est_composé_de_(fait_partie_de)/P46_est_composé·e_de_(fait_partie_de)/g
s/P46 est composé de/P46_est_composé·e_de/g
s/P48 a pour identifiant préférentiel (est l'identifiant préférentiel de)/P48_a_pour_identifiant_préférentiel_(est_l’identifiant_préférentiel_de)/g
s/P49 a pour responsable actuel·le ou antérieur·e (est responsable actuel·le ou antérieur·e de)/P49_a_pour_détenteur·rice_actuel·le_ou_antérieur·e_(est_détenteur·rice_actuel·le_ou_antérieur·e_de)/g
s/P49_a_pour_responsable_actuel·le_ou_antérieur·e_(est_responsable_actuel·le_ou_antérieur·e_de)/P49_a_pour_détenteur·rice_actuel·le_ou_antérieur·e_(est_détenteur·rice_actuel·le_ou_antérieur·e_de)/g
s/P50 a pour responsable actuel·le (est responsable actuel·le de)/P50_a_pour_détenteur·rice_actuel·le_(est_détenteur·rice_actuel·le_de)/g
s/P50_a_pour_responsable_actuel·le_(est_responsable_actuel·le_de)/P50_a_pour_détenteur·rice_actuel·le_(est_détenteur·rice_actuel·le_de)/g
s/P51 a pour propriétaire actuel·le ou antérieur·e (est propriétaire actuel·le ou antérieur·e de)/P51_a_pour_propriétaire_actuel·le_ou_antérieur·e_(est_propriétaire_actuel·le_ou_antérieur·e_de)/g
s/P52 a pour propriétaire actuel·le (est propriétaire actuel·le de)/P52_a_pour_propriétaire_actuel·le_(est_propriétaire_actuel·le_de)/g
s/P53 a pour localisation actuelle ou antérieure (est localisation actuelle ou antérieure de)/P53_a_pour_localisation_actuelle_ou_antérieure_(est_la_localisation_actuelle_ou_antérieure_de)/g
s/P53_a_pour_localisation_actuelle_ou_antérieure_(est_localisation_actuelle_ou_antérieure_de)/P53_a_pour_localisation_actuelle_ou_antérieure_(est_la_localisation_actuelle_ou_antérieure_de)/g
s/P53 a pour localisation actuelle ou antérieure/P53_a_pour_localisation_actuelle_ou_antérieure/g
s/P53i a pour localisation actuelle ou antérieure/P53i_est_localisation_actuelle_ou_antérieure_de/g
s/P54_a_actuellement_pour_localisation_fixe_(est_actuellement_localisation_fixe_de)/P54_a_actuellement_pour_localisation_fixe_(est_actuellement_la_localisation_fixe_de)/g
s/P54_a_actuellement_pour_localisation_fixe_(est_actuellement_localisation_fixe_de)/P54_a_actuellement_pour_localisation_fixe_(est_actuellement_la_localisation_fixe_de)/g
s/P55 a actuellement pour localisation (est actuellement localisation de)/P55_a_actuellement_pour_localisation_(est_actuellement_la_localisation_de)/g
s/P55_a_actuellement_pour_localisation_(est_actuellement_localisation_de)/P55_a_actuellement_pour_localisation_(est_actuellement_la_localisation_de)/g
s/P56 présente pour caractéristique (se trouve sur)/P56_présente_comme_caractéristique_(se_trouve_sur)/g
s/P57_a_pour_nombre_de_parties/P57_a_pour_nombre_de_parties/g
s/P59 a pour section (se situe sur ou dans)/P59_a_pour_zone_(se_situe_sur_ou_dans)/g
s/P59 a pour section/P59_a_pour_zone/g
s/P59i est localisé à ou s'insère dans/P59i_se_site_sur_ou_dans/g
s/P62 illustre (est illustré par)/P62_illustre_(est_illustré_par)/g
s/P62 figure (est figuré sur)/P62_illustre_(est_illustré_par)/g
s/P62_illustre_(est_illustré_par)/P62_dépeint_(est_dépeint_par)/g
s/P62_dépeint_(est_dépeint_par)/P62_dépeint_(est_dépeint·e_par)/g
s/P65 présente comme visuel (est présenté par)/P65_porte_l’entité_visuelle_(est_l’entité_visuelle_portée_par)/g
s/P65 présente l’item visuel (est présenté par)/P65_représente_l’entité_visuelle_(est_représenté_par)/g
s/P65 représente l'entité visuelle (est représenté par)/P65_représente_l’entité_visuelle_(est_représenté_par)/g
s/P65 représente l’entité visuelle (est représenté par)/P65_représente_l’entité_visuelle_(est_représenté_par)/g
s/P65_représente_l'entité_visuelle_(est_représenté_par)/P65_représente_l’entité_visuelle_(est_représenté_par)/g
s/P65_représente_l’entité_visuelle_(est_représenté_par)/P65_porte_l’entité_visuelle_(est_l’entité_visuelle_portée_par)/g
s/P65 représente l'entité visuelle/P65_porte_l’entité_visuelle/g
s/P67 renvoie à (fait l'objet d'un renvoi par)/P67_renvoie_à_(fait_l’objet_d’un_renvoi_par)/g
s/P67_renvoie_à_(fait_l’objet_d’un_renvoi_par)/P67_se_réfère_à_(fait_l’objet_d’une_référence_par)/g
s/P67_renvoie_à_(fait_l’objet_d’un_renvoi_par)/P67_fait_référence_à_(fait_l’objet_d’une_référence_par)/g
s/P67_se_réfère_à_(fait_l’objet_d’une_référence_par)/P67_fait_référence_à_(fait_l’objet_d’une_référence_par)/g
#s/P67_se_réfère_à_(fait_l’objet_d’une_référence_par)/P67_renvoie_à_(fait_l’objet_d’un_renvoi_par)/g
s/P68 prévoit l’emploi de (emploi prévu dans)/P68_prévoit_l’emploi_de_(emploi_prévu_dans)/g
s/P69 est associé à (est associé à)/P69_est_associé_à_(est_associé_à)/g
s/P69.1 a comme type/P69.1_a_comme_type/g
s/P70 documente (est_documenté dans)/P70_documente_(est_documenté_dans)/g
s/P71 liste (est listé dans)/P71_liste_(est_listé·e_dans)/g
s/P72 a pour langue (est la langue de)/P72_a_pour_langue_(est_la_langue_de)/g
s/P72_est_en_langue/P72_a_pour_langue_(est_la_langue_de)/g
s/P73 a pour traduction (est la traduction de)/P73_a_pour_traduction_(est_la_traduction_de)/g
s/P74 a pour résidence actuelle ou antérieure (est résidence actuelle ou antérieure de)/P74_réside_ou_a_résidé_à_(est_ou_a_été_la_résidence_de)/g
s/P75 est détenteur de (est détenu par)/P75_est_détenteur_de_(est_détenu_par)/g
s/P79 a son début qualifié par/P79_a_son_début_qualifié_par/g
s/P80 a sa fin qualifiée par/P80_a_sa_fin_qualifiée_par/g
s/P81 couvre au moins/P81_couvre_au_moins/g
s/P82 couvre au plus-ayant lieu durant/P82_couvre_au_plus-ayant_lieu_durant/g
s/P86 s’insère dans (inclut)/P86_s’insère_dans_(inclut)/g
s/P89 s’insère dans (contient)/P89_s’insère_dans_(contient)/g
s/P89_s’insère_dans_(contient)/P89_s’insère_dans_(inclut)/g
s/P92 a fait exister (a commencé à exister par)/P92_a_fait_exister_(a_commencé_à_exister_par)/g
s/P93_a_supprimé(a_été_supprimé⋅e_par)/P93_a_mis_fin_à_l’existence_de_(a_cessé_d’exister_du_fait_de)/g
s/P93 a mis fin à l’existence de (a cessé d’exister par)/P93_a_mis_fin_à_l’existence_de_(a_cessé_d’exister_du_fait_de)/g
s/P96 de mère (a donné naissance à)/P96_de_mère_(a_donné_naissance_à)/g
s/P97 de père (a été père pour)/P97_de_père_(a_été_père_pour)/g
s/P98 a donné vie à (est né)/P98_a_donné_vie_à_(est_né)/g
s/P99_a_dissous_(a_été_dissous_par)/P99_a_dissous_(a_été_dissous_par)/g
s/P100_a_été_la_mort_de_(est_mort_par)/P100_a_été_la_mort_de_(est_mort_par)/g
s/P101 a eu comme utilisation générale (a été l'utilisation de)/P101_a_eu_comme_utilisation_générale_(a_été_l'utilisation_de)/g
s/P101_a_eu_comme_utilisation_générale_(a_été_l'utilisation_de)/P101_a_eu_comme_usage_général_(a_été_l’usage_de)/g
s/P101_a_eu_pour_usage_général_(a_été_l'usage_général_de)/P101_a_eu_pour_usage_général_(a_été_l’usage_de)/g
s/P102 a pour titre (est titre de)/P102_a_pour_titre_(est_titre_de)/g
s/P103 a été fait pour (a été raison d’être de)/P103_a_été_fait·e_pour_(a_été_raison_d’être_de)/g
s/P103 a été fait pour (a été raison d'être de)/P103_a_été_fait·e_pour_(a_été_raison_d’être_de)/g
s/P103_a_été_fait·e_pour_(a_été_raison_d’être_de)/P103_a_été_fait·e_pour_(a_été_la_raison_d’être_de)/g
s/P104 est soumis.e à (s'applique à)/P104_est_soumis.e_à_(s’applique_à)/g
s/P106 est composé de (fait partie de)/P106_est_composé·e_de_(fait_partie_de)/g
s/P106 est composé·e de (fait partie de)/P106_est_composé·e_de_(fait_partie_de)/g
s/P107 a pour membre actuel ou ancien (est actuel ou ancien membre de)/P107_a_pour_membre_actuel_ou_ancien_(est_membre_actuel_ou_ancien_de)/g
s/P107.1 type de membre/P107.1_type_de_membre/g
s/P109 a pour conservateur·rice·x actuel·le ou antérieur·e (est conservateur·rice·x actuel·le ou antérieur·e de)/P109_a_pour_conservateur·rice·x_actuel·le_ou_antérieur·e_(est_conservateur·rice·x_actuel·le_ou_antérieur·e_de)/g
s/P109 a pour conservateur actuel ou antérieur (est conservateur actuel ou antérieur de)/P109_a_pour_conservateur·rice·x_actuel·le_ou_antérieur·e_(est_conservateur·rice·x_actuel·le_ou_antérieur·e_de)/g
s/P113 a retiré (a été retiré par)/P113_a_retiré_(a_été_retiré_par)/g
s/P121 recouvre partiellement (BROUILLON)/P121_recouvre_partiellement)/g
s/P121 recouvre partiellement/P121_recouvre_partiellement)/g
s/P122 est limitrophe/P122_est_limitrophe/g
s/P122 est limitrophe de/P122_est_limitrophe/g
s/P125 a mobilisé un objet du type (a été le type d'objet employé pour)/P125_a_mobilisé_un_objet_du_type_(a_été_le_type_d'objet_employé_pour)/g
s/P126 a employé (a été employé dans)/P126_a_employé_(a_été_employé_dans)/g
s/P127 a pour terme générique (a pour terme spécifique)/P127_a_pour_terme_générique_(a_pour_terme_spécifique)/g
s/P128 est support de (a pour support)/P128_est_support_de_(a_pour_support)/g
s/P128 est le support de (a pour support)/P128_est_support_de_(a_pour_support)/g
s/P129 a pour sujet (est sujet de)/P129_a_pour_sujet_(est_sujet_de)/g
s/P130 présente des caractéristiques de (a des caractéristiques également présentes sur)/P130_présente_des_caractéristiques_de_(a_des_caractéristiques_également_présentes_sur)/g
s/P130_présente_des_caractéristiques_de_(a_des_caractéristiques_également_présentes_sur)/P130_présente_des_caractéristiques_de_(a_des_caractéristiques_se_trouvant_aussi_sur)/g
s/P132 recouvrement spatio-temporel/P132_recouvrement_spatio-temporel/g
s/P134 a continué (a été continué par)/P134_a_continué_(a_été_continué_par)/g
s/P135 a créé le type (a été créé par)/P135_a_créé_le_type_(a_été_créé_par)/g
s/P138 represente/P138_représente/g
s/P138 représente (est représenté par)/P138_représente_(est_représenté_par)/g
s/P140 a affecté un attribut à (a reçu un attribut par)/P140_a_affecté_un_attribut_à_(a_reçu_un_attribut_par)/g
s/P141 a attribué (a été attribué par)/P141_a_attribué_(a_été_attribué_par)/g
s/P142 a mobilisé comme élément  (a été mobilisé dans)/P142_a_mobilisé_comme_élément__(a_été_mobilisé_dans)/g
s/P143 a fait entrer (est entré par)/P143_a_fait_entrer_(est_entré_par)/g
s/P144 a fait entrer dans (a gagné un membre du fait de)/P144_a_fait_entrer_dans_(a_gagné_un_membre_du_fait_de)/g
s/P144i a gagné un membre du fait de/P144i_a_gagné_un_membre_du_fait_de/g
s/P145 a fait sortir (est sorti du fait de)/P145_a_fait_sortir_(est_sorti_du_fait_de)/g
s/P148 a pour composant (est composant de)/P148_a_pour_composant_(est_composant_de)/g
s/P150 définit les parties typiques de (définit l'ensemble typique pour)/P150_définit_les_parties_typiques_de_(définit_l'ensemble_typique_pour)/g
s/P150 définit_les_parties_typiques_de_(définit_l'ensemble_typique_pour)/P150_définit_des_parties_typiques_de_(définit_des_ensembles_typiques_pour)/g
s/P151 a été formé·e de (a participé à)/P151_a_été_formé·e_de_(a_participé_à)/g
s/P152 a pour parent (est parent de)/P152_a_pour_parent_(est_parent_de)/g
s/P156 occupe (est occupé par)/P156_occupe_(est_occupé_par)/g
s/P156 occupe/P156_occupe/g
s/P156i est occupé par/P156i_est_occupé_par/g
s/P157 a pour référentiel spatial (est le référentiel spatial de)/P157_a_pour_référentiel_spatial_(est_le_référentiel_spatial_de)/g
s/P157 est à l’arrêt par rapport à/P157_a_pour_référentiel_spatial/g
s/P157i procure un espace de référence pour/P157i_est_le_référentiel_spatial_de/g
s/P161 a une projection spatiale/P161_a_pour_projection_spatiale/g
s/P161 a pour projection spatiale/P161_a_pour_projection_spatiale/g
s/P165_intègre (est_intégré_dans)/P165_intègre_(est_intégré_dans)/g
s/P165_contient_(est_contenu_dans)/P165_contient_(est_contenu·e_dans)/g
s/P168 lieu défini par (défini le lieu)/P168_lieu_défini_par_(défini_le_lieu)/g
s/P168 Lieu est défini par (défini le lieu)/P168_lieu_défini_par_(défini_le_lieu)/g
s/P170 définit le temps (temps défini par)/P170_définit_le_temps_(temps_défini_par)/g
s/P171 est inclue dans le lieu/P171_quelque_part_dans/g
s/P173 commence avant ou à la fin de (prend fin avec ou après le début de)/P173_commence_avant_ou_à_la_fin_de_(prend_fin_avec_ou_après_le_début_de)/g
s/P174 commence avant (commence après le début de)/P174_commence_avant_(commence_après_le_début_de)/g
s/P177 a attribué le type de propriété (est le type de propriété attribué)/P177_a_attribué_le_type_de_propriété_(est_le_type_de_propriété_attribué)/g
s/P179 a eu pour prix de vente (a été le prix de vente de)/P179_a_eu_pour_prix_de_vente_(a_été_le_prix_de_vente_de)/g
s/P187 a pour plan de production (est plan de production de)/P187_a_pour_plan_de_production_(est_plan_de_production_de)/g
s/P187_a_pour_plan_de_production_(est_plan_de_production_de)/P187_a_pour_plan_de_production_(est_le_plan_de_production_pour)/g
s/P187 a pour plan de production (est le plan de production pour)/P187_a_pour_plan_de_production_(est_le_plan_de_production_pour)/g
s/P188 nécessite l'outil de production (est l'outil de production de)/P188_nécessite_l'outil_de_production_(est_l'outil_de_production_de)/g
s/P188 nécessite l’outil de production (est l’outil de production pour)/P188_nécessite_l'outil_de_production_(est_l'outil_de_production_de)/g
s/P189 désigne approximativement (est désigné approximativement)/P189_désigne_approximativement_(est_désigné_approximativement)/g
s/P190 a pour contenu sympbolique/P190_a_pour_contenu_symbolique/g
s/P190 a pour contenu symbolique/P190_a_pour_contenu_symbolique/g
s/P191 a eu pour durée (était la durée de)/P191_a_eu_pour_durée_(était_la_durée_de)/g
s/P196 définit (est défini par)/P196_définit_(est_défini·e_par)/g
s/P196_définit_(est_défini_par)/P196_définit_(est_défini·e_par)/g
s/P196 définit/P196_définit/g
s/P198 contient ou supporte (est contenu ou supporté par)/P198_contient_ou_supporte_(est_contenu·e_ou_supporté·e_par)/g
