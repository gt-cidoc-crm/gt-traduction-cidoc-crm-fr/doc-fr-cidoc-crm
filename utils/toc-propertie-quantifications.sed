s/un vers un/un-à-un/g
s/un-vers-un/un-à-un/g
s/un à un/un-à-un/g
s/un-à-un/un-à-un/g
s/un à plusieurs/un-à-plusieurs/g
s/un vers plusieurs/un-à-plusieurs/g
s/plusieurs vers un/plusieurs-à-un/g
s/plusieurs à un/plusieurs-à-un/g
s/plusieurs à plusieurs/plusieurs-à-plusieurs/g
s/plusieurs vers plusieurs/plusieurs-à-plusieurs/g
s/plusieurs-à-plusieurs(0,n:0,n)/plusieurs-à-plusieurs (0,n:0,n)/g
s/plusieurs-à-plusieurs  (1,n,0,n)/plusieurs-à-plusieurs, nécessaire (1,n:0,n)/g
s/plusieurs-à-plusieurs, nécessaire  (1,n,0,n)/plusieurs-à-plusieurs, nécessaire (1,n:0,n)/g
s/plusieurs-à-plusieurs, nécessairement (1,n:0,n)/plusieurs-à-plusieurs, nécessaire (1,n:0,n)/g
s/plusieurs-à-une (0,1:0,n)/plusieurs-à-un (0,1:0,n)/g
s/un-à-plusieurs (0,n,0,n)/un-à-plusieurs (0,n:0,1)/g
s/un-à-plusieurs (1,n:1,1)/un-à-plusieurs (1,n:1,1)/g
s/un-à-plusieurs, nécessaire (1,n:0,n)/un-à-plusieurs, nécessaire (1,n:0,1)/g
s/une-à-plusieurs, nécessaire (1,n,0,1)/un-à-plusieurs, nécessaire (1,n:0,1)/g
s/0,1,0,n/0,1:0,n/g
s/0,n,0,n/0,n:0,n/g
s/0,n0,n/0,n:0,n/g
s/(0,n:0,n|/(0,n:0,n) |/g
s/1,n,0,n/1,n:0,n/g
s/1,1,0,n/1,1:0,n/g
s/plusieurs, (/plusieurs (/g
s/^| Quantification |/| Quantification : |/g


# plusieurs-à-plusieurs (0,n:0,n)
# un-à-plusieurs (0,n:0,1)
# plusieurs-à-un (0,1:0,n)
# plusieurs-à-plusieurs, nécessaire (1:n:0,n)
# un-à-plusieurs, nécessaire (1,n:0,1)
# plusieurs-à-un,nécessaire (1,1:0,n)
# un-à-plusieurs, dépendant (0,n:1,1)
# un-à-plusieurs, nécessaire, dépendant (1,n:1,1)
# plusieurs-à-un, nécessaire, dépendant (1,1:1,n)
# un-à-un (1,1:1,1)
# ?? P196 : un-à-un, nécessaire (1,1:0,1) ??
