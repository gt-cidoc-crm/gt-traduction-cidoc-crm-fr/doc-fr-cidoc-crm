> ### superproperty

>A superproperty is a property that is a generalization of one or more other properties (its subproperties), which means that it subsumes all instances of its subproperties, and that it can also have additional instances that do not belong to any of its subproperties. The intension of the superproperty is less restrictive than any of its subproperties. The subsumption relationship or generalization is the inverse of the IsA relationship or specialization. A superproperty may be a generalization of the inverse of another property.

### super-propriété 
Une superpropriété est une propriété qui est une généralisation d'une ou plusieurs autres propriétés (ses sous-propriétés), ce qui signifie qu'elle englobe toutes les instances de ses sous-propriétés et qu'elle peut également avoir des instances supplémentaires qui n'appartiennent à aucune de ses sous-propriétés. L'intension de la superpropriété est moins restrictive que n'importe laquelle de ses sous-propriétés. La relation de subsomption ou généralisation est l'inverse de la relation IsA ou spécialisation. Une superpropriété peut être une généralisation de l'inverse d'une autre propriété.
