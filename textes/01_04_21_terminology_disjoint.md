> ### disjoint 
>Classes are disjoint if the intersection of their extensions is an empty set. In other words, they have no common instances in any possible world.

### disjoint
Les classes sont disjointes si l'intersection de leurs extensions est un ensemble vide. En d'autres termes, elles n'ont pas d'instances communes dans tous les mondes possibles.
