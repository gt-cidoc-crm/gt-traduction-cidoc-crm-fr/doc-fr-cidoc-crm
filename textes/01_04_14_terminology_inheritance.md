> ### inheritance
>Inheritance of properties from superclasses to subclasses means that if an item x is an instance of a class A, then:
>    1 all properties that must hold for the instances of any of the superclasses of A must also hold for item x, and
>    2 all optional properties that may hold for the instances of any of the superclasses of A may also hold for item x.

### héritage
L'héritage des propriétés des superclasses vers les sous-classes signifie que si un élément x est une instance d'une classe A, alors : <br>1 toutes les propriétés qui doivent s'appliquer aux instances de l'une des superclasses de A doivent également s'appliquer à l'élément x, et <br>2 toutes les propriétés optionnelles qui peuvent s'appliquer aux instances de l'une des superclasses de A peuvent également s'appliquer à l'élément x.
