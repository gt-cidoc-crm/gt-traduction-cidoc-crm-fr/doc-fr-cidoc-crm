> #### About the logical expressions used in the CIDOC CRM
>The present CIDOC CRM specifications are annotated with logical axioms, providing an additional formal expression of the CIDOC CRM ontology. This section briefly introduces the assumptions that are at the basis of the logical expression of the CIDOC CRM (for a fully detailed account of the logical expression of semantic data modelling, see (Reiter,1984)).
>In terms of semantic data modelling, classes and properties are used to express ontological knowledge by means of various kinds of constraints, such as sub-class/sub-property links, e.g., E21 Person is a sub-class of E20 Biological Object, or domain/range constraints, e.g., the domain of P152 has parent is class E21 Person.
>In contrast, first-order logic-based knowledge representation relies on a language for formally encoding an ontology. This language can be directly put in correspondence with semantic data modelling in a straightforward way:
>    • classes are named by unary predicate symbols; conventionally, we use E21 as the unary predicate symbol corresponding to class E21 Person;
>    • properties are named by binary predicate symbols; conventionally, we use P152 as the binary predicate symbol corresponding to property P152 has parent.
>    • properties of properties, “.1 properties” are named by ternary predicate symbols; conventionally, we use P14.1 as the ternary predicate symbol corresponding to property P14.1 in the role of.
>Ontology is expressed in logic by means of logical axioms, which correspond to the constraints of semantic modelling. In the definition of classes and properties of the CIDOC CRM the axioms are placed under the heading ‘In first order logic’. There are several options for writing statements in first order logic. In this document we use a standard compact notation widely used in text books and scientific papers. The definition is given in the table below.
>Table 1: Symbolic Operators in First Order Logic Representation
>| Symbol | Name | reads | Truth value |
>|--------|------|-------|-------------|
>|Operators        |      |       |             |
>| ∧ | conjunction | and | (φ ∧ ψ) is true if and only if both φ and ψ are true|
>| ∨ | disjunction | or | (φ ∨ ψ) is true if and only if at least one of either φ or ψ is true |
>| ¬ | negation | not | ¬φ is true if and only if φ is false |
>| ⇒ | implication | implies, if … then ... | (φ ⇒ ψ) is true if and only if it is not the case that φ is true and ψ is false |
>| ⇔ | equivalence | is equivalent to, if … and only if … | φ ⇔ ψ is true if and only if both φ and ψ are true or both φ and ψ are false |
>| Quantifiers | |||
>| ∃ | existential quantifier | exists, there exists at least one |
>| ∀ | Universal quantifier | forall, for all ||

>For instance, the above sub-class link between E21 Person and E20 Biological Object can be formulated in first order logic as the axiom:
>(∀x) [E21(x) ⇒E20(x)]
>(reading: for all individuals x, if x is a E21 then x is an E20). 
>In the definitions of classes and properties in this document the universal quantifier(s) are omitted for simplicity, so the above axiom is simply written:
>E21(x) ⇒E20(x)
>Likewise, the above domain constraint on property P152 has parent can be formulated in first order logic as the axiom:
>P152(x,y) ⇒E21(x)
>(reading: for all individuals x and y, if x is a P152 of y, then x is an E21).
>Properties of properties, indicated by a '.1' after the property number are described as ternary predicate symbols. For example, the property P14.1 in the role of  is described as the ternary predicate symbol corresponding to property P14 carried out by (performed) :
>P14(x,y) ⇒ E7(x)
>P14(x,y)⇒ E39(y)
>P14(x,y,z) ⇒ [P14(x,y) ∧ E55(z)]
>These basic considerations should be used by the reader to understand the logical axioms that are used into the definition of the classes and properties. Further information about the first order formulation of CIDOC CRM can be found in (Meghini & Doerr, 2018)

### À propos des expressions logiques utilisées dans le CIDOC CRM
Les présentes spécifications du CIDOC CRM sont annotées d'axiomes logiques, fournissant une expression formelle supplémentaire de l'ontologie du CIDOC CRM. Cette section présente brièvement les hypothèses qui sont à la base de l'expression logique du CIDOC CRM (pour un compte rendu détaillé de l'expression logique de la modélisation sémantique des données, voir (Reiter, 1984)).<br>Le CRM CIDOC est exprimé en termes de primitives de la modélisation sémantique des données. En tant que tel, il est constitué de :<ul><li>des classes, qui représentent des notions générales dans le domaine du discours, comme la classe CIDOC CRM E21 Personne qui représente la notion de personne ;</li><li>des propriétés, qui représentent les relations binaires qui relient les individus dans le domaine du discours, comme la propriété CIDOC CRM P152 has parent qui relie une personne à l'un de ses parents</li></ul>.<br>Les classes et les propriétés sont utilisées pour exprimer les connaissances ontologiques au moyen de divers types de contraintes, comme les liens entre sous-classes et sous-propriétés, par exemple E21 Personne est une sous-classe de E20 Objet biologique, ou les contraintes de domaine et de gamme, par exemple le domaine de P152 a un parent est la classe E21 Personne.<br>En revanche, la représentation des connaissances basée sur la logique du premier ordre s'appuie sur un langage permettant d'encoder formellement une ontologie. Ce langage peut être mis en correspondance avec la modélisation sémantique des données de manière directe et directe :<ul><li>les classes sont nommées par des symboles de prédicats unaires ; conventionnellement, nous utilisons E21 comme symbole de prédicat unaire correspondant à la classe E21 Personne ;</li><li>les propriétés sont nommées par des symboles de prédicat binaire ; par convention, nous utilisons P152 comme symbole de prédicat binaire correspondant à la propriété P152 a un parent.</li><li>les propriétés des propriétés, ".1 propriétés" sont nommées par des symboles prédicats ternaires ; par convention, nous utilisons P14.1 comme le symbole prédicat ternaire correspondant à la propriété P14.1 dans le rôle de.</li></ul><br>L'ontologie est exprimée en logique au moyen d'axiomes logiques, qui correspondent aux contraintes de la modélisation sémantique. Dans la définition des classes et des propriétés du CRM CIDOC, les axiomes sont placés sous la rubrique "En logique du premier ordre". Il existe plusieurs options pour écrire des déclarations en logique du premier ordre. Dans ce document, nous utilisons une notation compacte standard largement utilisée dans les manuels et les articles scientifiques. La définition est donnée dans le tableau ci-dessous.<br>Table 1: Représentation des opérateurs symboliques de logique de premier ordre.
| Symbole | Nom | se lit | valeur vraie |
|---|---|---|---|
| Opérateurs | | |
| ∧ | conjonction | et | (φ ∧ ψ) est vrai si et seulement si φ et ψ sont tous deux vrais |
| ∨ | disjonction | ou | (φ ∨ ψ) est vrai si et seulement si au moins l'un de φ ou ψ est vrai |
| ¬ | négation | non | ¬φ est vrai si et seulement si φ est faux |
| ⇒ | implication | implique, si … alors… | (φ ⇒ ψ) est vrai si et seulement si il n'est pas le cas que φ est vrai et ψ est faux |
| ⇔ | équivalence | est équivalent à, si… et si seulement… | φ ⇔ ψ est vrai si et seulement si φ et ψ sont vrais ou φ et ψ sont tous deux faux |
| Quantificateurs |||
| ∃ | quantificateur d'existence | il existe, il existe au moins un||
| ∀ | quantificateur universel | pour tout, pour tous ||

<p>Par exemple, le lien de sous-classe ci-dessus entre E21 Personne et E20 Objet biologique peut être formulé en logique du premier ordre comme l'axiome :<br>(∀x) [E21(x) ⇒E20(x)]<br>(lecture : pour tout individu x, si x est un E21 alors x est un E20). <br>Dans les définitions des classes et des propriétés de ce document, le(s) quantificateur(s) universel(s) sont omis par souci de simplicité, de sorte que l'axiome ci-dessus s'écrit simplement :<br>E21(x) ⇒E20(x)<br>De même, la contrainte de domaine ci-dessus sur la propriété P152 a parent peut être formulée en logique du premier ordre comme l'axiome :<br>P152(x,y) ⇒E21(x)<br>(lecture : pour tous les individus x et y, si x est un P152 de y, alors x est un E21).<br>Les propriétés des propriétés, indiquées par un ".1" après le numéro de la propriété, sont décrites comme des symboles prédicatifs ternaires. Par exemple, la propriété P14.1 dans le rôle de est décrite comme le symbole de prédicat ternaire correspondant à la propriété P14 exécutée par (réalisée) :<br>P14(x,y) ⇒ E7(x)<br>P14(x,y)⇒ E39(y)<br>P14(x,y,z) ⇒ [P14(x,y) ∧ E55(z)]<br>Ces considérations de base doivent être utilisées par le lecteur pour comprendre les axiomes logiques qui sont utilisés dans la définition des classes et des propriétés. De plus amples informations sur la formulation de premier ordre du CIDOC CRM sont disponibles dans (Meghini & Doerr, 2018).</p>
