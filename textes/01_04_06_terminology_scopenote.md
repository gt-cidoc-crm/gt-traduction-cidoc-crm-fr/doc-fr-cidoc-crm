> ### scope note
> A scope note is a textual description of the intension of a class or property.
> Scope notes are not formal modelling constructs, but are provided to help explain the intended meaning and application of the CIDOC CRM’s classes and properties. Basically, they refer to a conceptualisation common to domain experts and disambiguate between different possible interpretations. Illustrative example instances of classes and properties are also regularly provided in the scope notes for explanatory purposes.

### note d'application 
Une note d'application est une description textuelle de l'intension d'une classe ou d'une propriété. <br> Les notes d'application ne sont pas des constructions formelles du modèle, mais sont fournies pour aider à expliquer la signification et l'utilisation des classes et des propriétés du CIDOC CRM. Fondamentalement, elles se réfèrent à une conceptualisation commune d'experts du domaine et désambiguïsent les différentes interprétations possibles. Des exemples d'instances de classes et de propriétés sont également régulièrement fournies dans les notes d'application à des fins d'explication.
