> ### primitive 
>The term primitive as used in knowledge representation characterizes a concept that is declared and its meaning is agreed upon, but that is not defined by a logical deduction from other concepts. For example, mother may be described as a female human with child. Then mother is not a primitive concept. Event however is a primitive concept. 
>Most of the CIDOC CRM is made up of primitive concepts.

### primitive 
Le terme primitive, tel qu'il est utilisé dans la représentation des connaissances, caractérise un concept qui est déclaré et dont la signification est acceptée, mais qui n'est pas défini par une déduction logique à partir d'autres concepts. Par exemple, la mère peut être décrite comme un être humain de sexe féminin avec un enfant. Dans ce cas, la mère n'est pas un concept primitif. L'événement, en revanche, est un concept primitif. <br>La majeure partie du CIDOC CRM est constituée de concepts primitifs.
