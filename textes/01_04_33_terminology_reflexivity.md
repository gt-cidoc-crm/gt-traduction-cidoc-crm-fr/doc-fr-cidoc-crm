> ### reflexivity
>Reflexivity is defined in the standard way found in mathematics or logic: A property P is reflexive if the domain and range are the same class and for all instances x, of this class the following is the case: x is related by P to itself. The intention of a property as described in the scope note will decide whether a property is reflexive or not. An example of a reflexive property is E53 Place. P89 falls within (contains): E53 Place. 

### réfléxivité
La réflexivité est définie de la manière standard que l'on trouve en mathématiques ou en logique : Une propriété P est réflexive si le domaine et l'étendue sont la même classe et si, pour toutes les instances x de cette classe, le cas suivant se présente : x est lié par P à lui-même. L'intention d'une propriété, telle que décrite dans la note sur la portée, déterminera si une propriété est réflexive ou non. Un exemple de propriété réflexive est E53 Place. P89 fait partie de (contient) : E53 Place.
