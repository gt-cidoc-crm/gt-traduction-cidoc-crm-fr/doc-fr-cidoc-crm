> ### instance
>An instance of a class is a real-world item that fulfils the criteria of the intension of the class. Note, that the number of instances declared for a class in an information system is typically less than the total in the real world. For example, you are an instance of Person, but you are not mentioned in all information systems describing Persons.
>For example:
>The painting known as the “The Mona Lisa” is an instance of the class E22 Human-Made Object.
>An instance of a property is a factual relation between an instance of the domain and an instance of the range of the property that matches the criteria of the intension of the property.
>For example:
>The Mona Lisa has former or current owner. The Louvre is an instance of the property P51 has former or current owner (is former or current owner of).

### instance 
Une instance d'une classe est un élément du monde réel qui répond aux critères de l'intension de la classe. Notez que le nombre d'instances déclarées pour une classe dans un système d'information est généralement inférieur au nombre total d'instances dans le monde réel. Par exemple, vous êtes une instance de Personne, mais vous n'êtes pas mentionné dans tous les systèmes d'information décrivant des Personnes. <br>Par exemple :<br>Le tableau connu sous le nom de "La Joconde" est une instance de la classe E22 Objet anthropique.<br>Une instance d'une propriété est une relation factuelle entre une instance du domaine et une instance du co-domaine de la propriété qui correspond aux critères de l'intension de la propriété.<br>Par exemple : <br>La Joconde a un·e propriétaire actuel·le ou antérieur·e. Le Louvre est une instance de la propriété P51 a  un·e propriétaire actuel·le ou antérieur·e (est propriétaire actuel·le ou antérieur·e).
