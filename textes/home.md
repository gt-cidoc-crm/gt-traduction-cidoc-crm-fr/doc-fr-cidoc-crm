# Traduction Française du CIDOC CRM

Bienvenue sur ce site qui vous donne accès à la traduction française du CIDOC CRM.

Ce site est généré automatiquement (intégration continue) à partir du projet  
[https://gitlab.huma-num.fr/gt-cidoc-crm/gt-traduction-cidoc-crm-fr/doc-fr-cidoc-crm](https://gitlab.huma-num.fr/gt-cidoc-crm/gt-traduction-cidoc-crm-fr/doc-fr-cidoc-crm)

Pour en savoir plus sur ce projet, consulter la page [Le projet de traduction](le-projet-de-traduction) 

