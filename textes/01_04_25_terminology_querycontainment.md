> ### query containment
>Query containment is a problem from database theory: A query X contains another query Y, if for each possible population of a database the answer set to query X contains also the answer set to query Y. If query X and Y were classes, then X would be superclass of Y. 

### confinement de requêtes
Le confinement des requêtes est un problème issu de la théorie des bases de données : Une requête X contient une autre requête Y, si pour chaque population possible d'une base de données, l'ensemble des réponses à la requête X contient également l'ensemble des réponses à la requête Y. Si les requêtes X et Y étaient des classes, alors X serait une superclasse de Y.
