> ### extension
>The extension of a class is the set of all real-life instances belonging to the class that fulfil the criteria of its intension. This set is “open” in the sense that it is generally beyond our capabilities to know all instances of a class in the world and indeed that the future may bring new instances about at any time (Open World). An information system may at any point in time refer to some instances of a class, which form a subset of its extension.

### extension 
L'extension d'une classe est l'ensemble de toutes les instances réelles appartenant à la classe qui remplissent les critères de son intension. Cet ensemble est "ouvert" dans le sens où il est généralement impossible de connaître toutes les instances d'une classe dans le monde et que l'avenir peut apporter de nouvelles instances à tout moment (monde ouvert). Un système d'information peut, à tout moment, faire référence à certaines instances d'une classe, qui forment un sous-ensemble de son extension.
