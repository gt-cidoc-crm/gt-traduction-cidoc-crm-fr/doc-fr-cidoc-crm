> ### universal
>The fundamental ontological distinction between universals and particulars can be informally understood by considering their relationship with instantiation: particulars are entities that have no instances in any possible world; universals are entities that do have instances. Classes and properties (corresponding to predicates in a logical language) are usually considered to be universals. (after Gangemi et al. 2002, pp. 166-181).

### universaux
La distinction ontologique fondamentale entre les universaux et les particuliers peut être comprise de manière informelle en considérant leur relation avec l'instanciation : les particuliers sont des entités qui n'ont pas d'instances dans tout monde possible ; les universaux sont des entités qui ont des instances. Les classes et les propriétés (correspondant aux prédicats dans un langage logique) sont généralement considérées comme des universaux. (d'après Gangemi et al. 2002, pp. 166-181).
