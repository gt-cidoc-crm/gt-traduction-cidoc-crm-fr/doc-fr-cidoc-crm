# Le projet de traduction

## Objectif
L'objectif est de traduire en français la documentation du CIDOC CRM la plus à jour.

## Qui sommes-nous ?
Ce groupe a été formé suite à l'Ecole thématique "Données Interopérables pour le Patrimoine"  [DONIPAT](https://masa.hypotheses.org/donipat) organisée par le consortium MASA (octobre 2019).<br>
Il est constitué de chercheuses et ingénieurs de différents laboratoires et universités.<br>
Si vous souhaitez nous rejoindre, consulter la page [Comment participer](comment-participer)

## Quelle version ?
Nous avons commencé avec la version V6.2.7, disponible malheureusement qu'au [format docx](http://www.cidoc-crm.org/versions-of-the-cidoc-crm).<br/> 
La version V7.0 est disponible depuis le 20 juin 2020, puis la [version 7.1](http://www.cidoc-crm.org/sites/default/files/CIDOC%20CRM_v.7.1%20%5B8%20March%202021%5D.doc) est sortie le 8 mars 2021.<br/>
La [version 7.1.1](http://www.cidoc-crm.org/sites/default/files/cidoc_crm_version_7.1.1.docx) sortie en avril 2021, est devenu, quelques temps, la **version de référence** pour les traductions, d'autant qu'elle était la version "ultime" pour la demande de révision ISO… avant que la [version 7.1.2](https://cidoc-crm.org/Version/version-7.1.2) ne vienne compléter quelques propriétés (cf. [CR réunion du 25/2/2022](https://gitlab.huma-num.fr/gt-cidoc-crm/gt-traduction-cidoc-crm-fr/doc-fr-cidoc-crm/-/wikis/2022-02-25-Réunion))


Cette page reprend les informations disponibles dans le [README](https://gitlab.huma-num.fr/gt-cidoc-crm/gt-traduction-cidoc-crm-fr/doc-fr-cidoc-crm/-/blob/translate/README.md) du projet.
