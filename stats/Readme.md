# Réalisation d'une routine `#bash` pour effectuer des statistiques sur les traductions
##### cc by @RKrummeich
##### Merci à @bdavid pour les *techniques en coulisses*
## Description [Work in Progress]
Du fait de la nature du document, la traduction collective du CIDOC CRM est complexe, nécessitant la fabrication d'un processus commun au court du temps.
Les choix sémantiques font l'objet de plusieurs espaces de dialogues. 
La définition des outils (git par exemple), syntaxes (français, mathématiques), formes (figures) ou langages informatiques (markdown, html, bash) ne suffit pas à répondre à la logique formelle associée aux classes et propriétés du CIDOC CRM; cette dernière impose une certaine cohérence dans la validation des traductions réalisées par des individus ou des groupes, fondée notamment sur deux concepts propres au CIDOC CRM : le **domaine** et la **spécialisation** - ou la relation **"IsA"** (des classes et des propriétés).
Par ailleurs, l'organisation du collectif décentralisé de travail appelle à l'élaboration d'un agenda commun, tenant compte de processus asynchrones (traductions individuelles ou de groupes autonomes) et synchrones (validation collective des traductions). Afin de tenter d'évaluer le temps nécessaire aux divers processus, le script évalue une **modalité de quantification** d'un temps nécessaire aux tâches de traduction fondée sur la note d'application des concepts.

### Sources textuelles Cidoc CRM
Deux documents sont mobilisés :
- une version d'édition du document de référence, ici la [version 7.1.1](https://cidoc-crm.org/sites/default/files/cidoc_crm_version_7.1.1.docx),
- une version structurée XML d'une partie du document de référence, [accessible ici](https://cidoc-crm.org/html/cidoc_crm_v7.1.1.xml)
En particulier, les tables 3 (`entities_hierarchy.csv`) & 4 (`properties_hierarchy.csv`) reprenant la hiérarchie des classes et des propriétés sont extraites du premier document. Le second document permet d'extraire le contenu des notes d'application de chaque classe ou propriété. Il est aussi le format standard d'édition interopérable du CIDOC CRM, utilisé pour la publication des traductions sur la plateforme du CRM SIG.
### Objectifs de mesures
Le document à traduire (puis à éditer) compte 232 pages dont plus de 70% est constitué d'un descriptif structuré des classes et propriétés. Pour les classes, la structure compte 6 items (*Sous-classe de, Super-classe de, Note d'application, Exemples, En première logique, Propriétés*), auxquels s'ajoutent pour les propriétés le *Domaine*, *Co-domaine* et la *Cardinalité*. 
### *Poids* de groupes ou sous-groupes de traductions
Compte-tenu de la structure descriptive des éléments à traduire, le temps de traduction et de validation est majoritairement consacré à la traduction et validation de la note d'application. En ce sens, le choix effectué ici est de mesurer un *poids* de la traduction ou validation à effectuer à partir du **nombre de mots de la note d'application associée** à la classe ou la propriété étudiée.

## En cours
### Evaluation du projet
A ce stade d'avancée, le résultat du script permet d'afficher le [schéma des groupes logiques](https://gitlab.huma-num.fr/gt-cidoc-crm/gt-traduction-cidoc-crm-fr/doc-fr-cidoc-crm/-/blob/translate/stats/milestones_logical_groups_hierarchy.md) constitués à partir des deux hypothèses :
- les classes sous-classes directes de la classe étudiée ("IsA"),
- les propriétés de chacune des classes du groupe constitué dont elles sont le domaine.
Le script produit des fichiers markdown utilisant le formalisme *mermaid* pour organiser l'information.
C'est à partir de ces schémas que les milestones du git susceptibles d'organiser le déroulé de la traduction et de la validation sont renseignés.
### Rédaction du README.md
En cours.
### Factorisation du code bash
En cours.
### Représentations graphiques
Les graphiques sont directement affichés dans le git en cliquant sur les fichiers `grapheEXX.md` du dossier `stats/logical_groups/`, voir par exemple, le graphe du groupe logique de [E55 Type](https://gitlab.huma-num.fr/gt-cidoc-crm/gt-traduction-cidoc-crm-fr/doc-fr-cidoc-crm/-/blob/translate/stats/logical_groups/graphE55.md).
Compte-tenu des modalités de regroupement, une classe (avec les propriétés associées dont elle est le domaine) est susceptible d'appartenir à plusieurs groupes logiques : l'organisation des graphes (et donc des milestones) tient compte de ce constat, en limitant l'affectation de la validation de la traduction d'une entité (et ses propriétés directes) à un seul groupe, même si celle-ci reste mentionnée dans le(s) groupe(s) logique(s) auquel(s) elle appartient.
### Intégration des indications de traduction/validation du GT (googledoc) - méthode en cours d'abandon
Le suivi de traduction et de validation des classes et propriétés fait l'objet d'un suivi initial sur un document partagé (un *spreadsheet* sur un dépôt Google), reprenant les labels organisant le tableau de bord du git.
Ce document (au format CSV) est inséré dans le script afin d'ajouter l'information sur le graphe de chaque groupe logique.
## Perspective : agenda dynamique de traduction/révision/validation
### Proposer un agenda de traduction et de validation : un modèle d'apprentissage en cours d'élaboration [TODO]
L'agenda actuel suit le graphe des groupes logiques.
### Une étape en cours : utiliser l'API gitlab pour le suivi de traduction/validation
voir le dossier `issues_flow_metadata`, en lien notamment avec #285
