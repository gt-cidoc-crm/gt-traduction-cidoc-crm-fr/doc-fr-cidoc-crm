```mermaid
classDiagram
direction TB
E11  <|-- E12 : IsA
E12 : Production
E12 :  4-En cours de revision (186 mots)
E12 <.. P108 : has domain
P108 : has produced 
P108 :  1-A traduire (65 mots)
E12 <.. P186 : has domain
P186 : produced thing of product type 
P186 :  1-A traduire (25 mots)
E11  <|-- E79 : IsA
E79 : Part Addition
E79 :  3-Traduit en attente de revision (191 mots)
E79 <.. P111 : has domain
P111 : added 
P111 :  1-A traduire (21 mots)
E79 <.. P110 : has domain
P110 : augmented 
P110 :  1-A traduire (69 mots)
E11  <|-- E80 : IsA
E80 : Part Removal
E80 :  1-A traduire (189 mots)
E80 <.. P113 : has domain
P113 : removed 
P113 :  1-A traduire (21 mots)
E80 <.. P112 : has domain
P112 : diminished 
P112 :  1-A traduire (62 mots)
```
