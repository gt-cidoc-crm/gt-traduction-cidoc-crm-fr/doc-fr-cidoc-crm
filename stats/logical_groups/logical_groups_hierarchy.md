```mermaid
classDiagram
direction TB
E1  <|-- E77 : IsA
E77  <|-- E39 : IsA
E77  <|-- E70 : IsA
E70  <|-- E72 : IsA
E72  <|-- E18 : IsA
E18  <|-- E19 : IsA
E19  <|-- E20 : IsA
E18  <|-- E24 : IsA
E18  <|-- E26 : IsA
E72  <|-- E90 : IsA
E90  <|-- E73 : IsA
E73  <|-- E31 : IsA
E73  <|-- E33 : IsA
E73  <|-- E36 : IsA
E90  <|-- E41 : IsA
E70  <|-- E71 : IsA
E71  <|-- E28 : IsA
E28  <|-- E89 : IsA
E28  <|-- E55 : IsA
E55  <|-- E58 : IsA
E1  <|-- E92 : IsA
E1  <|-- E54 : IsA
E1  <|-- E59 : IsA
E1  <|-- E2 : IsA
E2  <|-- E4 : IsA
E4  <|-- E5 : IsA
E5  <|-- E7 : IsA
E7  <|-- E8 : IsA
E7  <|-- E11 : IsA
E7  <|-- E13 : IsA
E7  <|-- E65 : IsA
E5  <|-- E63 : IsA
E5  <|-- E64 : IsA
```
