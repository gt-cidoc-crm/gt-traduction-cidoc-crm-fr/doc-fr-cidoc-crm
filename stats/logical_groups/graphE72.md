```mermaid
classDiagram
direction TB
E72  <|-- E18 : IsA
E18 : Physical Thing
E18 :  5-Valide (215 mots)
E18 <.. P44 : has domain
P44 : has condition 
P44 :  5-Valide (75 mots)
E18 <.. P45 : has domain
P45 : consists of 
P45 :  5-Valide (70 mots)
E18 <.. P49 : has domain
P49 : has former or current keeper 
P49 :  5-Valide (192 mots)
E18 <.. P50 : has domain
P50 : has current keeper 
P50 :  5-Valide (85 mots)
E18 <.. P51 : has domain
P51 : has former or current owner 
P51 :  5-Valide (99 mots)
E18 <.. P52 : has domain
P52 : has current owner 
P52 :  5-Valide (80 mots)
E18 <.. P53 : has domain
P53 : has former or current location 
P53 :  5-Valide (123 mots)
E18 <.. P156 : has domain
P156 : occupies 
P156 :   (382 mots)
E18 <.. P59 : has domain
P59 : has section 
P59 :   (96 mots)
E18 <.. P128 : has domain
P128 : carries 
P128 :  5-Valide (138 mots)
E18 <.. P46 : has domain
P46 : is composed of 
P46 :  5-Valide (185 mots)
E18 <.. P196 : has domain
P196 : defines 
P196 :  5-Valide (234 mots)
E18 <.. P198 : has domain
P198 : holds or supports 
P198 :  5-Valide (221 mots)
E72  <|-- E90 : IsA
E90 : Symbolic Object
E90 :  5-Valide (174 mots)
E90 <.. P190 : has domain
P190 : has symbolic content
P190 :  5-Valide (148 mots)
E90 <.. P106 : has domain
P106 : is composed of 
P106 :  5-Valide (41 mots)
```
