```mermaid
classDiagram
direction TB
E63  <|-- E67 : IsA
E67 : Birth
E67 :  3-Traduit en attente de revision (107 mots)
E67 <.. P96 : has domain
P96 : by mother 
P96 :  3-Traduit en attente de revision (92 mots)
E67 <.. P98 : has domain
P98 : brought into life 
P98 :  3-Traduit en attente de revision (62 mots)
E67 <.. P97 : has domain
P97 : from father 
P97 :  3-Traduit en attente de revision (106 mots)
E63  <|-- E81 : IsA
E81 : Transformation
E81 :  1-A traduire (212 mots)
E81 <.. P123 : has domain
P123 : resulted in 
P123 :  1-A traduire (59 mots)
E81 <.. P124 : has domain
P124 : transformed 
P124 :  1-A traduire (61 mots)
E63  <|-- E12 : IsA
E63  <|-- E65 : IsA
E63  <|-- E66 : IsA
```
